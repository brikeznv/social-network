<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-16" language="java" pageEncoding="UTF-16" %>
<html>
<head>
    <title>user</title>
    <link rel="stylesheet" type="text/css" HREF="<%=request.getContextPath()%>/resources/css/style.css" title="style"/>
    <script src="<c:url value="https://code.jquery.com/jquery-2.1.4.js"/>"></script>
    <%--<script src="https://code.jquery.com/jquery-2.1.4.js"></script>--%>

    <link href="/resources/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="/resources/bootstrap/css/bootstrap.css.map" rel="stylesheet">
</head>
<body>
<jsp:include page="/WEB-INF/jsp/templates/topPanel.jsp"/>

<div class="container">
    <jsp:include page="/WEB-INF/jsp/templates/${includeMid}"/>
</div>
</body>
</html>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>