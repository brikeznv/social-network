<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>

<!DOCTYPE html>

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<script src="<c:url value="https://code.jquery.com/jquery-2.1.4.js"/>"></script>

<link href="/resources/bootstrap/css/bootstrap.css" rel="stylesheet">
<link href="/resources/bootstrap/css/bootstrap.css.map" rel="stylesheet">


<nav class="navbar navbar-inverse" role="navigation">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/profile"><i class="glyphicon glyphicon-home"></i> Home</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="/friends">Friends</a></li>
                <li><a href="#">Messages</a></li>
                <li><a href = "/settings">Settings</a></li>
                <%--<li class="dropdown">--%>
                    <%--<a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>--%>
                    <%--<ul class="dropdown-menu">--%>
                        <%--<li><a href="#">Действие</a></li>--%>
                        <%--<li><a href="#">Другое действие</a></li>--%>
                        <%--<li><a href="#">Что-то еще</a></li>--%>
                        <%--<li class="divider"></li>--%>
                        <%--<li><a href="#">Отдельная ссылка</a></li>--%>
                        <%--<li class="divider"></li>--%>
                        <%--<li><a href="#">Еще одна отдельная ссылка</a></li>--%>
                    <%--</ul>--%>
                <%--</li>--%>
            </ul>
            <form class="navbar-form navbar-left" role="search">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Search">
                </div>
                <button type="submit" class="btn btn-default">Отправить</button>
            </form>
            <ul class="nav navbar-nav navbar-right">

                <li><a href="/login">Log in</a></li>
                <%--<li class="dropdown">--%>
                    <%--<a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>--%>
                    <%--<ul class="dropdown-menu">--%>
                        <%--<li><a href="#">Действие</a></li>--%>
                        <%--<li><a href="#">Другое действие</a></li>--%>
                        <%--<li><a href="#">Что-то еще</a></li>--%>
                        <%--<li class="divider"></li>--%>
                        <%--<li><a href="#">Отдельная ссылка</a></li>--%>
                    <%--</ul>--%>
                <%--</li>--%>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<c:url value="/resources/bootstrap/js/bootstrap.js"/>"></script>
<%--<script src="<c:url value="/resources/bootstrap/js/bootstrap.min.js"/>"></script>--%>
<%--<script src="<c:url value="/resources/bootstrap/js/npm.js"/>"></script>--%>