<%@ page contentType="text/html;charset=UTF-16" language="java" pageEncoding="UTF-16" %>

<div class="col-lg-2" style="position: fixed;">
    <ul class="nav navbar-inverse nav-stacked" style="margin-bottom: 10px;">
        <li class="active"><a href="#main" data-toggle="tab" aria-expanded="false">Main</a></li>
        <li class=""><a href="#security" data-toggle="tab" aria-expanded="true">Security</a></li>
        <link href="/resources/js/settings.js" rel="stylesheet">
    </ul>
</div>

<div id="myTabContent" class="tab-content">
    <div class="tab-pane fade active in" id="main">
        <div class="row">
            <div class="col-lg-4 col-lg-push-4 col-lg-pull-4">
                <div class="panel panel-default">
                    <div class="panel-heading">Change name</div>
                    <div class="panel-body">
                        <form class="form-inline" method="post">
                            <div class="input-group">
                                <input style="margin-bottom: 5px;" name="firstName" type="text" class="form-control"
                                       placeholder="First name...">
                            </div>

                            <div class="input-group">
                                <input style="margin-bottom: 5px;" name="lastName" type="text" class="form-control"
                                       placeholder="Last name...">
                            </div>

                            <div class="input-group">
                                <input style="margin-bottom: 5px;" name="nickName" type="text" class="form-control"
                                       placeholder="Nickname...">
                            </div>

                            <div>
                                <button class="btn btn-default" name="settingAction" value="name" type="submit">
                                    Submit
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-lg-push-4 col-lg-pull-4">
                <div class="panel panel-default">
                    <div class="panel-heading">Change password</div>
                    <div class="panel-body">
                        <form class="form-inline" method="post">
                            <div class="input-group">
                                <input style="margin-bottom: 5px;" name="password" type="text"
                                       class="form-control form-inline" placeholder="Your password...">
                            </div>

                            <div class="input-group">
                                <input style="margin-bottom: 5px;" name="newPassword" type="text" class="form-control"
                                       placeholder="New password...">
                            </div>

                            <div class="input-group">
                                <input style="margin-bottom: 5px;" name="confirmNewPassword" type="text"
                                       class="form-control" placeholder="Confirm new password...">
                            </div>

                            <div>
                                <button class="btn btn-default" name="settingAction" value="password" type="submit">
                                    Submit
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-lg-push-4 col-lg-pull-4">
                <div class="panel panel-default">
                    <div class="panel-heading">Change avatar</div>
                    <div class="panel-body">
                        <form class="form-inline" enctype="multipart/form-data">
                            <input id="lefile" name="image" type="file" style="display:none">

                            <div class="input-append">
                                <input id="photoCover" class="input-large" type="text">
                                <a class="btn" onclick="$('input[id=input-file]').click();">Browse</a>
                                <input class="btn btn-default" formmethod="POST" formaction="/uploadAvatar"
                                       type="submit" value="Upload picture">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="security" style="background-color: rgba(8, 8, 8, 0.3)">
        <div class="col-lg-4 col-lg-push-4 col-lg-pull-4">
            <div class="panel panel-default">
                <div class="panel-heading">Security settings</div>
                <div class="panel-body">
                    <form method="post">

                        <div class="form-group">
                            <label for="pageView">Who can see my page:</label>
                            <select class="form-control" id="pageView" name="pageView">
                                <option value="all">all</option>
                                <option value="registred">registred</option>
                                <option value="friends">friends</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="wallView">Who can see my wall:</label>
                            <select class="form-control" id="wallView" name="wallView">
                                <option value="all">all</option>
                                <option value="registred">registred</option>
                                <option value="friends">friends</option>
                                <option value="nobody">nobody</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="wallPost">Who can post wall messages:</label>
                            <select class="form-control" id="wallPost" name="wallPost">
                                <option value="all">all</option>
                                <option value="registred">registred</option>
                                <option value="friends">friends</option>
                                <option value="nobody">nobody</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="friendListView">Who can see my friends:</label>
                            <select class="form-control" id="friendListView" name="friendListView">
                                <option value="all">all</option>
                                <option value="registred">registred</option>
                                <option value="friends">friends</option>
                                <option value="nobody">nobody</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="messagePost">Who can send private messages:</label>
                            <select class="form-control" id="messagePost" name="messagePost">
                                <option value="all">all</option>
                                <option value="registred">registred</option>
                                <option value="friends">friends</option>
                                <option value="nobody">nobody</option>
                            </select>

                            <button name="settingAction" value="security" type="submit">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>