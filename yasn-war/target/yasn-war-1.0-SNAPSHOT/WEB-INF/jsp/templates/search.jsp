<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%--<%@ page contentType="text/html;charset=UTF-16" language="java" pageEncoding="UTF-16"%>--%>
<%--
  Created by IntelliJ IDEA.
  User: Sierra
  Date: 22.08.15
  Time: 14:05
  To change this template use File | Settings | File Templates.
--%>
<link href="/resources/js/search.js">

<div class="col-lg-12">
    <div class="row">
        <form method="get" class="form-group" id="search-form">
            <div class="input-group">
                <input name="searchQuery" class="form-control" id="search-input" placeholder="Type key words..."
                       value="${requestScope.searchQuery}" type="text">
                <span class="input-group-btn">
                    <button type="button" class="btn btn-default" id="search-button">Search</button>
                </span>
            </div>
            Gender
            <br>

            <div class="radio">
                <label><input name="gender" type="radio" value="m"/>Male</label>
            </div>
            <div class="radio">
                <label><input name="gender" type="radio" value="f"/>Female</label>
            </div>
            <div class="radio">
                <label><input name="gender" type="radio" value=""/>All</label>
            </div>
            <%--Add default value--%>
            <br>
            Age
            <br>
            <input name="ageFrom" placeholder="From" type="number">
            -
            <input name="ageTo" placeholder="To" type="number">
            <br>

        </form>
    </div>

    <div class="row" id="search-result">
        <jsp:include page="/WEB-INF/jsp/templates/profileList.jsp"/>
    </div>
</div>