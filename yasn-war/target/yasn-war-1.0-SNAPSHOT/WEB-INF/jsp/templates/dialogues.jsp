<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ftm" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--@elvariable id="privateMessageList" type="java.util.List<com.getjavajob.web05.brikezn.jpa.bean.PrivateMessage>"--%>
<%--
  Created by IntelliJ IDEA.
  User: Sierra
  Date: 17.11.15
  Time: 18:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<%--@elvariable id="friendList" type="java.util.List<com.getjavajob.webapp.brikezn.jsonbean.ProfileJson>"--%>
<c:forEach items="${friendList}" var="friend">
    <div class="media">
        <a class="pull-left" href="/profile/<c:out value="${friend.id}"/>">
            <img style="max-width: 64px" class="media-object" src="${friend.avatarHtmlValue}"
                 onerror="this.src = '/resources/img/profile/default-profile-image.png'">
        </a>

        <div class="media-body" style="background: rgba(24, 21, 21, 0.14); color: #f5f5f5">
            <h4 class="media-heading">
                <c:out value="${friend.firstName} ${friend.lastName}"/>
            </h4>
            <h6>
                <ftm:formatDate value="${privateMessage.date}" pattern="dd.MM.yyyy HH:mm:ss"/>
            </h6>
            <c:out value="${privateMessage.content}" escapeXml="false"/>
            <form>
                <button type="submit" class="btn btn-default" formmethod="get"
                        formaction="/dia?member=<c:out value="${friend.id}"/>">
                    Open dialogue
                </button>
            </form>
        </div>
    </div>
</c:forEach>
