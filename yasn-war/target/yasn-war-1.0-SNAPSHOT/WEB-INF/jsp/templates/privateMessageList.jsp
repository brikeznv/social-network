<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ftm" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: Sierra
  Date: 17.11.15
  Time: 19:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%--@elvariable id="privateMessageList" type="java.util.List<com.getjavajob.webapp.brikezn.jpa.bean.PrivateMessage>"--%>
<%--@elvariable id="profile" type="com.getjavajob.webapp.brikezn.jsonbean.ProfileJson"--%>
<%--@elvariable id="memberProfile" type="com.getjavajob.webapp.brikezn.jsonbean.ProfileJson"--%>

<link href="/resources/js/private-messages-pagination.js">

<div id="member" hidden><c:out value="${requestScope.memberProfile.id}"/></div>

<ul class="pagination" id="pagination">
    <c:forEach var="pageNumber" begin="1" end="${countOfPages}">
        <li>
            <a onclick="updateMessageList(<c:out value="${pageNumber}"/>)" id="page<c:out value="${pageNumber}"/>">
                <c:out value="${pageNumber}"/>
            </a>
        </li>
    </c:forEach>
</ul>

<%--<c:forEach items="${privateMessageList}" var="privateMessage">--%>
    <div class="media" id="privateMessages">
        <%--<a class="pull-left" href="/profile/<c:out value="${profile.id}"/>">--%>
            <%--<img style="max-width: 64px" class="media-object" src="${profile.avatarHtmlValue}"--%>
                 <%--onerror="this.src = '/resources/img/profile/default-profile-image.png'">--%>
        <%--</a>--%>

        <%--<div class="media-body" style="background: rgba(24, 21, 21, 0.14); color: #f5f5f5">--%>
            <%--<h4 class="media-heading">--%>
                <%--<c:out value="${privateMessage.sender.firstName} ${privateMessage.sender.lastName}"/>--%>
            <%--</h4>--%>
            <%--<h6>--%>
                <%--<c:out value="${privateMessage.date}"/>--%>
                <%--&lt;%&ndash;<ftm:formatDate value="${privateMessage.date}" pattern="dd.MM.yyyy HH:mm:ss"/>&ndash;%&gt;--%>
            <%--</h6>--%>
            <%--<c:out value="${privateMessage.content}" escapeXml="false"/>--%>
        <%--</div>--%>
    </div>
<%--</c:forEach>--%>

<form method="post">
    <div class="form-group">
        <div class="input-group">
            <input class="form-control" name="content" type="text" autofocus/>
            <span class="input-group-btn">
                <button class="btn btn-default" formmethod="post" type="submit">Send message</button>
            </span>
        </div>
    </div>
</form>


<script>
    //При клике на номер страницы:
    //1) изменить активный номер
    //2) послать ajax запрос с нужными сообщениями
    //3) удалить имеющиеся сообщения
    //4) добавить сообщения, пришедние с сервера

    $(document).ready(function () {
        updateMessageList(1);
    });

    function updateMessageList(activePage) {
        var privateMessages = document.getElementById("privateMessages");
        clearChildNodes(privateMessages);
        var data = getMessages(activePage);
    }

    function getMember() {
        var member = document.getElementById("member");
        return member.innerHTML;
    }

    function getMessages(pageNumber) {
        return $.ajax({
            type: 'GET',
            url: '/getPrivateMessages?page=' + pageNumber + '&member=' + getMember(),
            async: false,
            success: function (data) {
                var privateMessagesNode = document.getElementById("privateMessages");

                data.forEach(function (item) {
                    var node = createPrivateMessageNode(item);
                    privateMessagesNode.appendChild(node);
                });
            }
        })
    }

    function createPrivateMessageNode(item) {
        var message = document.createElement('div');
        message.className = 'media';
            var a = document.createElement('a')
            a.className = 'pull-left';
            a.href = '/profile/' + item.sender.id;
                var img = document.createElement('img');
                img.style = 'max-width: 64px';
                img.className = 'media-object';

                if (item.sender.avatarHtmlValue != null) {
                    img.src = item.sender.avatarHtmlValue;
                } else {
                    img.src = '/resources/img/profile/default-profile-image.png';
                }
            a.appendChild(img);
        message.appendChild(a);
            var body = document.createElement('div');
            body.className = 'media-body';
            body.style = 'background: rgba(24, 21, 21, 0.14); color: #f5f5f5';
                var h4 = document.createElement('h4');
                h4.className = 'media-heading';
                h4.innerHTML = item.sender.firstName + ' ' + item.sender.lastName;
            body.appendChild(h4);
                var content = document.createElement('div');
                content.innerHTML = item.content;
            body.appendChild(content);
        message.appendChild(body);

        return message;
    }

    function clearChildNodes(node) {
        while (child = node.firstChild) node.removeChild(child)
    }
</script>