/**
 * Created by Sierra on 27.09.15.
 */

$(document).ready(function () {
    var countrySelect = document.getElementById("country-list");
    var countryList = getCountries();
    fillLocationSelect(countrySelect, countryList);
});

$("#country-list").change(function () {
    var countryValue = document.getElementById("country-list").value;
    var citySelect = document.getElementById("city-list");
    var cityList = getCitiesByCountry(countryValue);
    fillLocationSelect(citySelect, cityList);
});

function getCountries() {
    return $.ajax({
        type: 'GET',
        url: '/getCountries',
        async: false,
        success: function (data) {
            return data;
        }
    })
}

function getRegions(countryId) {
    return $.ajax({
        type: 'GET',
        url: '/getRegions/' + countryId,
        async: false,
        success: function (data) {
            return data;
        }
    });
}

function getCitiesByRegion(regionId) {
    return $.ajax({
        type: 'GET',
        url: '/getCitiesByRegion/' + regionId,
        async: false,
        success: function (data) {
            return data;
        }
    });
}

function getCitiesByCountry(countryId) {
    return $.ajax({
        type: 'GET',
        url: '/getCitiesByCountry/' + countryId,
        async: false,
        success: function (data) {
            return data;
        }
    });
}

function fillLocationSelect(node, data) {
    removeChildren(node);

    data.responseJSON.forEach(function (item) {
        var option = createLocationOption(item);
        node.appendChild(option);
    });
}

function createLocationOption(item) {
    var option = document.createElement("option");
    option.value = item.id;
    option.innerHTML = item.title;

    return option;
}

function removeChildren(node) {
    while (child = node.firstChild) node.removeChild(child)
}