<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ftm" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-16" language="java" pageEncoding="UTF-16" %>

<c:choose>
    <c:when test="${accessParamMap.wallPost == true}">
        <form method="post">
            <div class="form-group">
                <div class="input-group">
                    <input class="form-control" name="wallMessage" type="text"/>
                    <span class="input-group-btn">
                      <button class="btn btn-default" formmethod="post" type="submit">Send message</button>
                    </span>
                </div>
            </div>
        </form>
    </c:when>
</c:choose>


<%--@elvariable id="profile" type="com.getjavajob.webapp.brikezn.jpa.bean.Profile"--%>
<c:forEach items="${profile.wallMessageList}" var="wallMessage">
    <div class="media">
        <a class="pull-left" href="/profile/<c:out value="${wallMessage.sender.id}"/>">
            <img style="max-width: 64px" class="media-object" src="${wallMessage.id.sender.avatar.htmlValue}"
                 onerror="this.src = '/resources/img/profile/default-profile-image.png'">
        </a>

        <div class="media-body" style="background: rgba(24, 21, 21, 0.14); color: #f5f5f5">
            <h4 class="media-heading">
                <c:out value="${wallMessage.id.sender.firstName} ${wallMessage.id.sender.lastName}"/>
            </h4>
            <h6>
                <ftm:formatDate value="${wallMessage.date}" pattern="dd.MM.yyyy HH:mm:ss"/>
            </h6>
            <c:out value="${wallMessage.content}" escapeXml="false"/>
        </div>
    </div>
</c:forEach>