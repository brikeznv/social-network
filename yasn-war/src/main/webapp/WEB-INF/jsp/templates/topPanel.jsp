<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-16" language="java" pageEncoding="UTF-16" %>

<meta http-equiv="X-UA-Compatible" content="IE=edge">

<nav class="navbar navbar-inverse" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/profile"><i class="glyphicon glyphicon-home"></i> Home</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="/friends">Friends</a></li>
                <li><a href="/dialogues">Messages</a></li>
                <li><a href="/settings">Settings</a></li>
            </ul>
            <form class="navbar-form navbar-left" role="search">
                <div class="form-group">
                    <input type="text" name="searchQuery" class="form-control" placeholder="Search">
                </div>
                <button type="submit" formaction="/search" formmethod="get" class="btn btn-default">Search</button>
            </form>
            <ul class="nav navbar-nav navbar-right">
                <c:if test="${sessionScope.id != null}">
                    <li><a href="/logout">Log out</a></li>
                </c:if>
                <c:if test="${sessionScope.id == null}">
                    <li><a href="/logout">Log in</a></li>
                </c:if>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>
