<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-16" language="java" pageEncoding="UTF-16" %>

<c:choose>
    <c:when test="${accessParamMap.pageView == false}">
        <div class="jumbotron">
            <h2>The page is hidden</h2>

            <p>...</p>
                <%--<p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a></p>--%>
        </div>
    </c:when>
    <c:otherwise>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                <img class="media-object img-responsive" src="${profile.avatar.htmlValue}"
                     onerror="this.src = '/resources/img/profile/default-profile-image.png'">
            </div>
            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                <div style="color: #f5f5f5">
                    <h3>
                            <%--@elvariable id="profile" type="com.getjavajob.webapp.brikezn.jpa.bean.Profile"--%>
                        <c:out value="${profile.firstName} ${profile.lastName}"/>
                    </h3>
                    <h5>
                        <c:out value="Birthday: ${profile.birthday}"/> <br/>
                        <c:out value="Location: ${profile.city.country.title}, ${profile.city.title}"/>
                    </h5>
                </div>
            </div>
        </div>
        <c:choose>
            <c:when test="${accessParamMap.friendListView == true}">
                <div class="row">
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Friends
                            </div>
                            <div class="panel-body">
                                <c:forEach items="${profile.friends}" var="friend" begin="0" end="5" step="1">
                                    <img class="media-object" style="width: 40px" src="${friend.avatar.htmlValue}"
                                         onerror="this.src = '/resources/img/profile/default-profile-image.png'">
                                    <c:out value="${friend.firstName} ${friend.lastName}"/>
                                </c:forEach>
                            </div>
                        </div>
                    </div>
                    <c:choose>
                        <c:when test="${accessParamMap.wallView == true}">
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">

                                <jsp:include page="/WEB-INF/jsp/templates/wall.jsp"/>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <%--<div class="jumbotron">--%>
                            <%--<h1>User would prefer to hide your page</h1>--%>
                            <%--<p>...</p>--%>
                            <%--&lt;%&ndash;<p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a></p>&ndash;%&gt;--%>
                            <%--</div>--%>
                        </c:otherwise>
                    </c:choose>
                </div>
            </c:when>
        </c:choose>
    </c:otherwise>
</c:choose>