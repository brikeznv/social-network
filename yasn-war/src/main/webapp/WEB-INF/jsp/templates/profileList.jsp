<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-16" language="java" pageEncoding="UTF-16" %>

<%--@elvariable id="profileList" type="java.util.List<com.getjavajob.webapp.brikezn.jpa.bean.Profile>"--%>
<c:forEach items="${profileList}" var="profile">
    <div class="col-xs-4 col-md-3 col-lg-2">
        <div class="thumbnail">
            <img class="img-responsive" src="${profile.avatar.htmlValue}"
                 onerror="this.src = '/resources/img/profile/default-profile-image.png'">

            <div class="caption">
                <h5>
                        ${profile.firstName} ${profile.lastName}
                </h5>
                    <%--<p>...</p>--%>
                <p>
                    <a href="/profile/<c:out value="${profile.id}"/>"
                       class="btn btn-default" role="button">Profile</a>
                </p>
            </div>
        </div>
    </div>
</c:forEach>