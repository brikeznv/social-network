<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>

<!DOCTYPE html>

<meta http-equiv="X-UA-Compatible" content="IE=edge">

<link href="<c:url value="<%=request.getContextPath()%>/resources/bootstrap/css/bootstrap.css">" rel="stylesheet">
    <link href="<c:url
            value="<%=request.getContextPath()%>/resources/bootstrap/css/bootstrap.css.map">" rel="stylesheet">

        <nav class="navbar navbar-inverse" role="navigation">
        <div class="container-fluid">
        <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/profile"><i class="glyphicon glyphicon-home"></i> Home</a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
        <li><a href="/friends">Friends</a></li>
        <li><a href="#">Messages</a></li>
        <li><a href = "/settings">Settings</a></li>
        </ul>
        <form class="navbar-form navbar-left" role="search">
        <div class="form-group">
        <input type="text" class="form-control" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-default">Отправить</button>
        </form>
        <ul class="nav navbar-nav navbar-right">
        <li><a href="/login">Log in</a></li>
        </ul>
        </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
        </nav>

        <script src="<c:url value="<%=request.getContextPath()%>/resources/bootstrap/js/bootstrap.js"/>"></script>
        <script src="<c:url value="<%=request.getContextPath()%>/resources/js/jquery-2.1.4.js">"></script>