<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-16" language="java" pageEncoding="UTF-16" %>
<html>
<head>
    <title>profile</title>
    <link rel="stylesheet" type="text/css" HREF="<%=request.getContextPath()%>/resources/css/style.css" title="style"/>
    <link href="<c:url value='<%=request.getContextPath() + "/resources/bootstrap/css/bootstrap.css"%>'/>"
          rel="stylesheet">
    <link href="<c:url value='<%=request.getContextPath() + "/resources/bootstrap/css/bootstrap.css.map"%>'/>"
          rel="stylesheet">
</head>
<body>
<jsp:include page="/WEB-INF/jsp/templates/topPanel.jsp"/>
<div class="container">
    <jsp:include page="/WEB-INF/jsp/templates/${includeMid}"/>
</div>
</body>
</html>

<script src="<c:url value='<%=request.getContextPath() + "/resources/js/jquery-2.1.4.js"%>'/>"></script>