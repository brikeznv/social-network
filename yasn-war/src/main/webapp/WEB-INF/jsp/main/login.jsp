<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-16" language="java" pageEncoding="UTF-16" %>

<html>
<head>
    <title>Sign in</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/style.css"/>"/>

    <link href="<c:url value='<%=request.getContextPath() + "/resources/bootstrap/css/bootstrap.css"%>'/>"
          rel="stylesheet">
</head>
<body style="background: url(<%=request.getContextPath()%>/resources/img/background/main/login.jpg) no-repeat center center fixed;">
<nav class="navbar navbar-inverse" role="navigation">
    <div class="container-fluid">
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/registration">Registration</a></li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>
<div class="container">
    <div class="col-xs-4 col-xs-push-4">
        <form id="form-login" class="form-signin" role="form" method="post">
            <h2 class="form-signin-heading" style="color:#c7ddef">Please sign in</h2>
            <input style="margin-bottom: 5px" name="email" type="email" class="form-control" placeholder="Email address"
                   required="" autofocus="">
            <input style="margin-bottom: 5px" name="password" type="password" class="form-control"
                   placeholder="Password" required="">
            <button id="submit-login" class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
        </form>

        <div id="alert" class="alert alert-warning alert-dismissible" role="alert" style="display: none">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                    aria-hidden="true">&times;</span></button>
            <strong>Error!</strong>

            <div id="error-message"></div>
        </div>
    </div>
</div>
</body>
</html>

<script src="<c:url value='<%=request.getContextPath() + "/resources/js/jquery-2.1.4.js"%>'/>"></script>
<script src="<c:url value='<%=request.getContextPath() + "/resources/bootstrap/js/bootstrap.js"%>'/>"></script>