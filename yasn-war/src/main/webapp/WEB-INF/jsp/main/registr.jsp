<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-16" language="java" pageEncoding="UTF-16" %>
<html>
<head>
    <title>Sign in</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" type="text/css"
          href="<c:url value='<%=request.getContextPath() + "/resources/css/style.css"%>'/>"/>
    <link href="<c:url value='<%=request.getContextPath() + "/resources/bootstrap/css/bootstrap.css"%>'/>"
          rel="stylesheet">
    <link href="<c:url value='<%=request.getContextPath() + "/resources/js/location-select-fill.js"%>'/>"
          rel="stylesheet">
</head>
<body>
<nav class="navbar navbar-inverse" role="navigation">
    <div class="container-fluid">
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/login">Login</a></li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>
<div class="container">
    <div class="col-xs-4 col-xs-push-4">
        <form class="form-signin" role="form" method="post">
            <h2 class="form-signin-heading" style="color:#c7ddef">Registration form</h2>
            <input style="margin-bottom: 5px" name="email" type="email" class="form-control" placeholder="Email address"
                   required autofocus value=
            <%--@elvariable id="paramMap" type="java.util.HashMap"--%>
            <c:if test="${paramMap.email != null}">
                <c:out value="${paramMap.email}"/>
            </c:if>>
            <input style="margin-bottom: 5px" name="password" type="password" class="form-control"
                   placeholder="Password" required>
            <input style="margin-bottom: 5px" name="confirmPassword" type="password" class="form-control"
                   placeholder="Confirm password"
                   required>


            <input style="margin-bottom: 5px" name="firstName" type="text" class="form-control" placeholder="First name"
                   required value=
            <%--@elvariable id="paramMap" type="java.util.HashMap"--%>
            <c:if test="${paramMap.firstName != null}">
                <c:out value="${paramMap.firstName}"/>
            </c:if>>
            <input style="margin-bottom: 5px" name="lastName" type="text" id="inputLastName" class="form-control"
                   placeholder="Last name"
                   required value=
            <%--@elvariable id="paramMap" type="java.util.HashMap"--%>
            <c:if test="${paramMap.lastName != null}">
                <c:out value="${paramMap.lastName}"/>
            </c:if>>
            <input style="margin-bottom: 5px" name="nickname" type="text" id="inputNickName" class="form-control"
                   placeholder="*Nickname" value=
            <%--@elvariable id="paramMap" type="java.util.HashMap"--%>
            <c:if test="${paramMap.nickname != null}">
                <c:out value="${paramMap.nickname}"/>
            </c:if>>

            <div style="color:white">
                <input name="gender" type="radio" class="form-radio" value="m">Male <br/>
                <input name="gender" type="radio" class="form-radio" value="f">Female <br/>

                BirthDay
            </div>
            <input style="margin-bottom: 5px" name="birthday" type="date" class="form-control" placeholder="Birthday"
                   value=
                   <%--@elvariable id="paramMap" type="java.util.HashMap"--%>
                   <c:if test="${paramMap.birthday != null}">
                       <c:out value="${paramMap.birthday}"/>
            </c:if>>

            Location
            <select style="margin-bottom: 5px;" name="countryId" class="form-control" id="country-list" size="1"
                    class="selectpicker">

            </select>

            <select style="margin-bottom: 5px;" name="cityId" class="form-control" id="city-list" size="1"
                    class="selectpicker">

            </select>

            <button class="btn btn-lg btn-primary btn-block" type="submit">Send form</button>
        </form>
    </div>
</div>
</body>
</html>

<script src="<c:url value='<%=request.getContextPath() + "/resources/js/jquery-2.1.4.js"%>'/>"></script>
<script src="<c:url value='<%=request.getContextPath() + "/resources/bootstrap/js/bootstrap.js"%>'/>"></script>