/**
 * Created by Sierra on 27.09.15.
 */

var makeProfileInfoNode = function (profile) {
    var avatar = document.createElement('img');
    avatar.className = 'image-responsive';

    if (profile.avatarHtmlValue != null) {
        avatar.src = profile.avatarHtmlValue;
    } else {
        avatar.src = "/resources/img/profile/default-profile-image.png";
    }

    var h5 = document.createElement('h5');
    h5.innerHTML = profile.firstName + " " + profile.lastName;

    var a = document.createElement('a');
    a.href = "/profile/" + profile.id;
    a.className = "btn btn-default";
    a.role = "button";
    a.innerHTML = "Profile";

    var profileInfoNode = document.createElement('div');
    profileInfoNode.className = "col-xs-4 col-md-3 col-lg-2";

    var caption = document.createElement("div");
    caption.className = 'caption';

    var thumbnail = document.createElement('div');
    thumbnail.className = 'thumbnail';

    caption.appendChild(h5);
    caption.appendChild(a);
    thumbnail.appendChild(avatar);
    thumbnail.appendChild(caption);
    profileInfoNode.appendChild(thumbnail);

    return profileInfoNode;
};

var removeChildren = function (node) {
    while (child = node.firstChild) node.removeChild(child)
};

function throttle(func, ms) {

    var isThrottled = false,
        savedArgs,
        savedThis;

    function wrapper() {

        if (isThrottled) { // (2)
            savedArgs = arguments;
            savedThis = this;
            return;
        }

        func.apply(this, arguments); // (1)

        isThrottled = true;

        setTimeout(function () {
            isThrottled = false; // (3)
            if (savedArgs) {
                wrapper.apply(savedThis, savedArgs);
                savedArgs = savedThis = null;
            }
        }, ms);
    }

    return wrapper;
}

function search() {
    var formParam = $("#search-form").serialize();

    $.ajax({
        type: 'GET',
        url: '/searchResult',
        async: true,
        data: formParam,
        success: function (data) {
            var resultNode = document.getElementById("search-result");
            removeChildren(resultNode);

            data.forEach(function (item) {
                var node = makeProfileInfoNode(item);
                resultNode.appendChild(node);
            });

            if (resultNode.firstChild == null) {
                var noVal = document.createElement("strong");
                noVal.innerHTML = "No profiles found...";
                resultNode.appendChild(noVal);
            }
        }
    })
}

$('#search-form').on('change, keyup', throttle(search, 1000));