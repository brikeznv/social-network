//При клике на номер страницы:
//1) изменить активный номер
//2) послать ajax запрос с нужными сообщениями
//3) удалить имеющиеся сообщения
//4) добавить сообщения, пришедние с сервера

$(document).ready(function () {
    var activePageNumber = getActivePageNumber();
    updateMessageList(activePageNumber);
});

function updateMessageList(activePage) {
    var privateMessages = document.getElementById("privateMessages");
    clearChildNodes(privateMessages);
    var data = getMessages(activePage);


}

function getMember() {
    var get = location.search;

    if(get != '') {
        tmp = (get.substr(1)).split('&');
        for(var i = 0; i < tmp.length; i++) {
            tmp2 = tmp[i].split('=');
            param[tmp2[0]] = tmp2[1];
        }

        return param['member'];
    }
}

function getMessages(pageNumber) {
    return $.ajax({
        type: 'GET',
        url: '/getMessages?page=' + pageNumber + '?member=' + getMember(),
        async: false,
        success: function (data) {
            var privateMessagesNode = document.getElementById("privateMessages");

            data.forEach(function (item) {
                var node = createPrivateMessageNode(item);
                privateMessagesNode.appendChild(node);
            });
        }
    })
}

function createPrivateMessageNode(item) {
    var h4 = document.createElement('h5');
    h4.innerHTML = item.content;
}

function clearChildNodes(node) {
    while (child = node.firstChild) node.removeChild(child)
}