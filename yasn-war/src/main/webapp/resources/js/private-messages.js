$(document).ready(function () {
    updateMessageList(1);
});

function updateMessageList(activePage) {
    var privateMessages = document.getElementById("privateMessages");
    clearChildNodes(privateMessages);
    var data = getMessages(activePage);
}

function getMember() {
    var member = document.getElementById("member");
    return member.innerHTML;
}

function getMessages(pageNumber) {
    return $.ajax({
        type: 'GET',
        url: '/getPrivateMessages?page=' + pageNumber + '&member=' + getMember(),
        async: false,
        success: function (data) {
            var privateMessagesNode = document.getElementById("privateMessages");

            data.forEach(function (item) {
                var node = createPrivateMessageNode(item);
                privateMessagesNode.appendChild(node);
            });
        }
    })
}

function createPrivateMessageNode(item) {
    var message = document.createElement('div');
    message.className = 'media';
    var a = document.createElement('a')
    a.className = 'pull-left';
    a.href = '/profile/' + item.sender.id;
    var img = document.createElement('img');
    img.style = 'max-width: 64px';
    img.className = 'media-object';

    if (item.sender.avatarHtmlValue != null) {
        img.src = item.sender.avatarHtmlValue;
    } else {
        img.src = '/resources/img/profile/default-profile-image.png';
    }
    a.appendChild(img);
    message.appendChild(a);
    var body = document.createElement('div');
    body.className = 'media-body';
    body.style = 'background: rgba(24, 21, 21, 0.14); color: #f5f5f5';
    var h4 = document.createElement('h4');
    h4.className = 'media-heading';
    h4.innerHTML = item.sender.firstName + ' ' + item.sender.lastName;
    body.appendChild(h4);
    var content = document.createElement('div');
    content.innerHTML = item.content;
    body.appendChild(content);
    message.appendChild(body);

    return message;
}

function clearChildNodes(node) {
    while (child = node.firstChild) node.removeChild(child)
}