/**
 * Created by Sierra on 27.09.15.
 */


$('input[id=input-file]').change(function () {
    $('#photoCover').val($(this).val());
});

$(window).load(function () {

    $.ajax({
        type: 'GET',
        url: '/getSecuritySettings',
        async: true,
        dataType: "json",
        success: function (data) {
            var select;

            select = document.getElementById("pageView");
            updateSecurityView(select, data.pageView);

            select = document.getElementById("wallView");
            updateSecurityView(select, data.wallView);

            select = document.getElementById("wallPost");
            updateSecurityView(select, data.wallPost);

            select = document.getElementById("friendListView");
            updateSecurityView(select, data.friendListView);

            select = document.getElementById("messagePost");
            updateSecurityView(select, data.messagePost);
        }
    });

    function updateSecurityView(select, actual) {
        var options = select.getElementsByTagName("option");
        var i;

        for (i = 0; i < options.length; i++) {
            var item = options[i];
            item.removeAttribute("selected");

            if (item.value == actual) {
                console.log("true");
                item.setAttribute("selected", "true");
            }
        }
    }
});