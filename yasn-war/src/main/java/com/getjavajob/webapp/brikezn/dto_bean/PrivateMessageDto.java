package com.getjavajob.webapp.brikezn.dto_bean;

import com.getjavajob.webapp.brikezn.jpa.bean.PrivateMessage;

import java.math.BigInteger;
import java.util.LinkedList;
import java.util.List;

public class PrivateMessageDto {

    private BigInteger id;
    private ProfileDto sender;
    private ProfileDto recipient;
    private String content;
    private BigInteger date;

    public PrivateMessageDto() {

    }

    public PrivateMessageDto(PrivateMessage message) {
        this.id = message.getId();
        this.sender = new ProfileDto(message.getSender());
        this.recipient = new ProfileDto(message.getRecipient());
        this.content = message.getContent();
        this.date = message.getDate();
    }

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public ProfileDto getSender() {
        return sender;
    }

    public void setSender(ProfileDto sender) {
        this.sender = sender;
    }

    public ProfileDto getRecipient() {
        return recipient;
    }

    public void setRecipient(ProfileDto recipient) {
        this.recipient = recipient;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public BigInteger getDate() {
        return date;
    }

    public void setDate(BigInteger date) {
        this.date = date;
    }

    public static List<PrivateMessageDto> toJson(List<PrivateMessage> privateMessageList) {

        LinkedList<PrivateMessageDto> privateMessageDtoList = new LinkedList<>();

        for (PrivateMessage message : privateMessageList) {
            privateMessageDtoList.add(new PrivateMessageDto(message));
        }

        return privateMessageDtoList;
    }

}
