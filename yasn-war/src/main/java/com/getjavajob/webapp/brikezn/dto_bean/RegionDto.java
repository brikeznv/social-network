package com.getjavajob.webapp.brikezn.dto_bean;

import com.getjavajob.webapp.brikezn.jpa.bean.location.Region;

public class RegionDto {

    private Integer id;
    private String title;

    public RegionDto() {

    }

    public RegionDto(Region region) {
        this.id = region.getId();
        this.title = region.getTitle();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}