package com.getjavajob.webapp.brikezn.spring.web.controller;

import com.getjavajob.webapp.brikezn.jpa.bean.PrivateMessage;
import com.getjavajob.webapp.brikezn.jpa.bean.Profile;
import com.getjavajob.webapp.brikezn.dto_bean.PrivateMessageDto;
import com.getjavajob.webapp.brikezn.dto_bean.ProfileDto;
import com.getjavajob.webapp.brikezn.service.PrivateMessageService;
import com.getjavajob.webapp.brikezn.service.ProfileService;
import com.getjavajob.webapp.brikezn.service.RelationshipService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Controller
public class PrivateMessagesController {

    @Autowired
    private PrivateMessageService privateMessageService;

    @Autowired
    private ProfileService profileService;

    @Autowired
    private RelationshipService relationshipService;

    @Autowired
    ProfileDto profile;

    public PrivateMessagesController() {

    }

    protected PrivateMessagesController(PrivateMessageService privateMessageService, ProfileService profileService,
                                        RelationshipService relationshipService, ProfileDto profile) {
        this.privateMessageService = privateMessageService;
        this.profileService = profileService;
        this.relationshipService = relationshipService;
        this.profile = profile;
    }

    @RequestMapping(path = "/dialogues", method = RequestMethod.GET, params = "member")
    public ModelAndView getPrivateMessages(@RequestParam String member) {

        Map<String, Object> model = new HashMap<>();
        Integer profileId = profile.getId();

        if (member == null || member.length() < 1) {
            Set<Profile> friends = relationshipService.getFriends(profileId);
            model.put("friendList", friends);
            model.put("includeMid", "../templates/dialogues.jsp");
            return new ModelAndView("profile", model);
        }

        Integer memberId = Integer.valueOf(member);
        ProfileDto profile = new ProfileDto(profileService.get(profileId));
        ProfileDto memberProfile = new ProfileDto(profileService.get(memberId));
        List<PrivateMessage> privateMessageList = privateMessageService.get(profileId, memberId);
        model.put("privateMessageList", privateMessageList);
        model.put("countOfPages", getCountOfPages(privateMessageList.size()));
        model.put("profile", profile);
        model.put("memberProfile", memberProfile);
        model.put("includeMid", "../templates/privateMessageList.jsp");

        return new ModelAndView("profile", model);
    }

    @RequestMapping(path = "/dialogues", method = RequestMethod.GET)
    public ModelAndView getDialogues() {

        Map<String, Object> model = new HashMap<>();
        Integer profileId = profile.getId();
        Set<Profile> friends = relationshipService.getFriends(profileId);
        Set<ProfileDto> friendsJson = ProfileDto.convertToJson(friends);

        model.put("friendList", friendsJson);
        model.put("includeMid", "../templates/dialogues.jsp");

        return new ModelAndView("profile", model);
    }

    @RequestMapping(path = "/dialogues", method = RequestMethod.POST, params = "member")
    public String postPrivateMessage(@RequestParam Map<String, Object> reqParam) {

        Object senderId = profile.getId();
        Object recipientId = reqParam.get("member");
        Object date = System.currentTimeMillis();
        Object content = reqParam.get("content");
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("senderId", senderId);
        paramMap.put("recipientId", recipientId);
        paramMap.put("content", content);
        paramMap.put("date", date);
        privateMessageService.save(paramMap);
        Integer memberId = Integer.valueOf((String) reqParam.get("member"));

        return "redirect:/dialogues?member=" + memberId;
    }

    private int getCountOfPages(int countOfMessages) {

        return countOfMessages / PrivateMessageService.COUNT_OF_MESSAGES_ON_PAGE +
                ((countOfMessages % PrivateMessageService.COUNT_OF_MESSAGES_ON_PAGE) == 0 ? 0 : 1);
    }

    @RequestMapping(path = "/getPrivateMessages", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<PrivateMessageDto> getSearchResult(@RequestParam String member, @RequestParam String page) {

        Integer profileId = profile.getId();
        Integer memberId = Integer.valueOf(member);
        Integer pageNumber = Integer.valueOf(page);

        return PrivateMessageDto.toJson(privateMessageService.get(profileId, memberId, pageNumber));
    }

}
