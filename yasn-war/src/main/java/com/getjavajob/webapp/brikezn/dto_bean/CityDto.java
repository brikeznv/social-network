package com.getjavajob.webapp.brikezn.dto_bean;

import com.getjavajob.webapp.brikezn.jpa.bean.location.City;

public class CityDto {

    private Integer id;
    private String title;

    public CityDto() {

    }

    public CityDto(City city) {
        this.id = city.getId();
        this.title = city.getTitle();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}