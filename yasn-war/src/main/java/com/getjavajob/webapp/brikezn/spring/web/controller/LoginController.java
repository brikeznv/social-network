package com.getjavajob.webapp.brikezn.spring.web.controller;

import com.getjavajob.webapp.brikezn.jpa.bean.Profile;
import com.getjavajob.webapp.brikezn.jpa.bean.location.City;
import com.getjavajob.webapp.brikezn.jpa.bean.location.Country;
import com.getjavajob.webapp.brikezn.dto_bean.ProfileDto;
import com.getjavajob.webapp.brikezn.service.ProfileDtoService;
import com.getjavajob.webapp.brikezn.service.ProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Controller
@SessionAttributes({"id"})
public class LoginController {

    @Autowired
    private ProfileService profileService;

    @Autowired
    public ProfileDto profile;

    public LoginController() {

    }

    protected LoginController(ProfileService profileService) {
        this.profileService = profileService;
    }

    @RequestMapping(path = "/login", method = RequestMethod.GET)
    public String login(@ModelAttribute Integer id) {

        if (id == null) {
            return "login";
        } else {
            if (profile != null && profile.getId() == null) {
                Profile profile = profileService.get(id);
                ProfileDtoService.fillProfileBean(this.profile, profile);
            }

            return "redirect:/profile";
        }
    }

    @RequestMapping(path = "/login", method = RequestMethod.POST)
    public String login(HttpServletRequest req, HttpSession session) {

        Object id = session.getAttribute("id");

        if (id != null) {
            return "redirect:/profile";
        }

        final String email = req.getParameter("email");
        Profile profile = profileService.get(email);
        session.setAttribute("id", profile.getId());
        ProfileDtoService.fillProfileBean(this.profile, profile);
        final String path = (String) session.getAttribute("path");

        if (path != null && path.length() > 0) {
            return "redirect:/" + path;
        }

        return "redirect:/profile/" + profile.getId();
    }

    @RequestMapping(path = "/logout", method = RequestMethod.GET)
    public String logOut(HttpSession session) {

        session.invalidate();

        return "redirect:/login";
    }

    @RequestMapping(path = "/loginErrors", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getLoginError(@RequestParam Map<String, String> requestParam) {

        final String email = requestParam.get("email");
        final String password = requestParam.get("password");
        Profile profile = profileService.get(email);

        if (profile == null) {
            return "No such email!";
        } else if (!profile.getPassword().equals(password)) {
            return "Incorrect password!";
        }

        return "";
    }
}