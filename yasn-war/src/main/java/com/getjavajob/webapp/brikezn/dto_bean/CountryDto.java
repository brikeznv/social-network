package com.getjavajob.webapp.brikezn.dto_bean;

import com.getjavajob.webapp.brikezn.jpa.bean.location.Country;

public class CountryDto {

    private Integer id;
    private String title;

    public CountryDto() {

    }

    public CountryDto(Country country) {
        this.id = country.getId();
        this.title = country.getTitle();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}