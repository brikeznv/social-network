package com.getjavajob.webapp.brikezn.spring.web.controller;

import com.getjavajob.webapp.brikezn.dto_bean.ProfileDto;
import com.getjavajob.webapp.brikezn.jpa.bean.location.City;
import com.getjavajob.webapp.brikezn.service.LocationService;
import com.getjavajob.webapp.brikezn.service.ProfileService;
import com.getjavajob.webapp.brikezn.service.SettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class RegistrationController {

    @Autowired
    ProfileService profileService;

    @Autowired
    LocationService locationService;

    @Autowired
    SettingService settingService;

    @Autowired
    ProfileDto profile;

    public RegistrationController() {

    }

    protected RegistrationController(ProfileService profileService, LocationService locationService,
                                     SettingService settingService, ProfileDto profile) {
        this.profileService = profileService;
        this.locationService = locationService;
        this.settingService = settingService;
        this.profile = profile;
    }

    @RequestMapping(path = "/registration", method = RequestMethod.GET)
    public ModelAndView getRegistration() {

        Map<String, Object> model = new HashMap<>();
        final List countryList = locationService.getAllCountries();
        model.put("countryList", countryList);

        return new ModelAndView("registr", model);
    }

    @RequestMapping(path = "/registration", method = RequestMethod.POST)
    public ModelAndView postRegistration(@RequestParam Map<String, Object> paramMap, HttpSession session) {

        Map<String, Object> model = new HashMap<>();
        Map<String, String> regErrorMap = getErrors(paramMap);

        if (regErrorMap.isEmpty()) {
            addCityParam(paramMap);
            Integer registredProfileId = profileService.saveOrUpdate(paramMap);
            settingService.createSecuritySettings(registredProfileId);
            session.setAttribute("id", registredProfileId);

            return new ModelAndView("redirect:/profile");
        } else {
            model.put("regErrorMap", regErrorMap);
            model.put("paramMap", paramMap);

            return new ModelAndView("registr", model);
        }
    }

    protected Map<String, String> getErrors(Map<String, Object> paramMap) {

        String email = (String) paramMap.get("email");
        String password = (String) paramMap.get("password");
        String confirmPassword = (String) paramMap.get("confirmPassword");
        Map<String, String> regErrorMap = new HashMap<>();

        if (profileService.get(email) != null) {
            regErrorMap.put("emailError", "email already exists");
        }

        if (!password.equals(confirmPassword)) {
            regErrorMap.put("confirmPasswordError", "password does not confirmed");
        }

        return regErrorMap;
    }

    private void addCityParam(Map<String, Object> paramMap) {

        String cityId = (String) paramMap.get("cityId");

        if (cityId != null) {
            City city = locationService.getCity(Integer.parseInt(cityId));

            if (city != null) {
                paramMap.put("city", city);
            }
        }
    }
}