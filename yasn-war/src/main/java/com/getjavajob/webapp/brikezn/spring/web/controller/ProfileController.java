package com.getjavajob.webapp.brikezn.spring.web.controller;

import com.getjavajob.webapp.brikezn.dto_bean.ProfileDto;
import com.getjavajob.webapp.brikezn.jpa.bean.Profile;
import com.getjavajob.webapp.brikezn.jpa.bean.SecuritySetting;
import com.getjavajob.webapp.brikezn.service.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

@Controller
public class ProfileController {

    private static final Logger LOGGER = Logger.getLogger(ProfileController.class);

    @Autowired
    private ProfileService profileService;

    @Autowired
    private SettingService settingService;

    @Autowired
    private WallService wallService;

    @Autowired
    private RelationshipService relationshipService;

    @Autowired
    ProfileDto profile;

    public ProfileController() {

    }

    protected ProfileController(ProfileService profileService, SettingService settingService, WallService wallService,
                                RelationshipService relationshipService, ProfileDto profile) {
        this.profileService = profileService;
        this.settingService = settingService;
        this.wallService = wallService;
        this.relationshipService = relationshipService;
        this.profile = profile;
    }


    @RequestMapping(path = "/profile", method = RequestMethod.GET)
    public String getProfile() {
        Integer id = profile.getId();

        return id == null ? "redirect:/login" : "redirect:/profile/" + id;
    }

    @RequestMapping(path = "/profile/{profileId}", method = RequestMethod.GET)
    public ModelAndView getProfile(@PathVariable String profileId) {

        Map<String, Object> model = new HashMap<>();
        Object loggedUserId = profile.getId();

        if (profileId == null) {
            if (loggedUserId != null) {
                return new ModelAndView("redirect:/profile/" + loggedUserId);
            } else {
                return new ModelAndView("redirect:/login");
            }
        }

//        if (profile == null) {
//            return new ModelAndView("redirect:/404");
//        }

        if (loggedUserId != null && loggedUserId.equals(profileId)) {
            model.put("mainPage", "true");
        } else {
            Map<String, Boolean> accessParamMap =
                    settingService.getAccessParameters((Integer) loggedUserId, Integer.parseInt(profileId));
            model.put("accessParamMap", accessParamMap);
        }

        model.put("includeMid", "profileInfo.jsp");
        model.put("profile", profileService.get(Integer.parseInt(profileId)));

        return new ModelAndView("profile", model);
    }

    @RequestMapping(path = "/profile/{profileId}", method = RequestMethod.POST)
    public String postProfile(@PathVariable String profileId, @RequestParam String wallMessage) {

        Map<String, Object> wallMessageParamMap = new HashMap<>();
        wallMessageParamMap.put("sender", profileService.get(profile.getId()));
        wallMessageParamMap.put("recipient", profileService.get(Integer.valueOf(profileId)));
        wallMessageParamMap.put("date", new Date(System.currentTimeMillis()));
        wallMessageParamMap.put("content", wallMessage);
        wallService.addWallMessage(wallMessageParamMap);

        return "redirect:/profile/" + profileId;
    }

    @RequestMapping(path = "/settings", method = RequestMethod.GET)
    public ModelAndView settings() {

        Map<String, Object> model = new HashMap<>();
        Integer loggedUserId = profile.getId();

        if (loggedUserId == null) {
            return new ModelAndView("redirect:/login");
        } else {
            model.put("logged", "true");
        }

        model.put("includeMid", "profileSettings.jsp");
        return new ModelAndView("profile", model);
    }

    @RequestMapping(path = "/uploadAvatar", method = RequestMethod.POST)
    public ModelAndView changeProfileImage(@RequestParam("image") MultipartFile file) {

        try (InputStream in = file.getInputStream()) {
            Integer id = profile.getId();
            profileService.updateProfileImage(id, in);
        } catch (IOException e) {
            LOGGER.error(e);
        }

        Map<String, Object> model = new HashMap<>();
        model.put("logged", "true");
        model.put("includeMid", "profileSettings.jsp");

        return new ModelAndView("profile", model);
    }

    @RequestMapping(path = "/settings", method = RequestMethod.POST)
    public ModelAndView settingsUpdate(@RequestParam Map<String, Object> settingParamMap) {

        Integer id = profile.getId();
        settingParamMap.put("id", id);

        switch ((String) settingParamMap.get("settingAction")) {
            case "profile":
                profileService.saveOrUpdate(settingParamMap);
                break;
            case "security":
                settingService.updateProfileSecurity(settingParamMap);
                break;
        }

        Map<String, Object> model = new HashMap<>();
        model.put("logged", "true");
        model.put("includeMid", "profileSettings.jsp");

        return new ModelAndView("profile", model);
    }

    @RequestMapping(path = "/friends")
    public String getFriendList() {

        Integer id = profile.getId();
        return id == null ? "redirect:/login" : "redirect:/friends/" + id;
    }

    @RequestMapping(path = "/friends/{profileId}")
    public ModelAndView getProfileFriendList(@PathVariable String profileId) {

        Map<String, Object> model = new HashMap<>();
        Object loggedUserId = profile.getId();
        Map<String, Boolean> accessParamMap = settingService.getAccessParameters((Integer) loggedUserId, Integer.parseInt(profileId));
        model.put("accessParamMap", accessParamMap);
        Integer profileIdNum = Integer.valueOf(profileId);
        Set<Profile> friendList = relationshipService.getFriends(profileIdNum);
        model.put("profileList", friendList);
        model.put("includeMid", "../templates/profileList.jsp");

        return new ModelAndView("profile", model);
    }

    @RequestMapping(path = "/getSecuritySettings", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, String> getSecuritySettings() {

        Integer id = profile.getId();
        Profile profile = profileService.get(id);
        List<SecuritySetting> settings = profile.getSecuritySettingList();
        Map<String, String> json = new HashMap<>();

        for (SecuritySetting setting : settings) {
            String settingName = setting.getId().getSecurityType().getName();
            String settingValue = setting.getValue().getName();
            json.put(settingName, settingValue);
        }

        return json;
    }

    @RequestMapping(path = "/getxmlprofile", method = RequestMethod.GET)
    @ResponseBody
    public String getXMLProfile() {
        Integer id = profile.getId();
        Profile profile = profileService.get(id);
        return profileService.getXMLFromBean(new ProfileDto(profile));
    }
}