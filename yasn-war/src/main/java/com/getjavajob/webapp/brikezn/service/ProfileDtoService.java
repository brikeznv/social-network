package com.getjavajob.webapp.brikezn.service;

import com.getjavajob.webapp.brikezn.dto_bean.ProfileDto;
import com.getjavajob.webapp.brikezn.jpa.bean.Profile;
import com.getjavajob.webapp.brikezn.jpa.bean.location.City;
import com.getjavajob.webapp.brikezn.jpa.bean.location.Country;

import java.util.Date;

/**
 * Created by Sierra.
 */
public class ProfileDtoService {

    public static void fillProfileBean(ProfileDto dto, Profile profile) {
        dto.setId(profile.getId());
        dto.setFirstName(profile.getFirstName());
        dto.setLastName(profile.getLastName());
        dto.setEmail(profile.getEmail());
        String nickname = profile.getNickname();

        if (nickname != null) {
            dto.setNickname(nickname);
        }

        Date birthday = profile.getBirthday();

        if (birthday != null) {
            dto.setBirthday(birthday);
        }

        City city = profile.getCity();

        if (city != null) {
            dto.setCity(city.getTitle());

            Country country = city.getCountry();

            if (country != null) {
                dto.setCountry(country.getTitle());
            }
        }

        String gender = profile.getGender();

        if (gender != null) {
            dto.setGender(gender);
        }
    }
}
