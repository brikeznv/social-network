package com.getjavajob.webapp.brikezn.spring.web.controller;

import com.getjavajob.webapp.brikezn.dto_bean.ProfileDto;
import com.getjavajob.webapp.brikezn.jpa.bean.Profile;
import com.getjavajob.webapp.brikezn.service.ProfileService;
import com.getjavajob.webapp.brikezn.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Controller
public class SearchController {

    @Autowired
    SearchService searchService;

    @Autowired
    ProfileService profileService;

    public SearchController() {

    }

    protected SearchController(SearchService searchService, ProfileService profileService) {
        this.searchService = searchService;
        this.profileService = profileService;

    }

    @RequestMapping(path = "/search", method = RequestMethod.GET)
    public ModelAndView getSearch(@RequestParam Map<String, String> requestParam) {

        List<Profile> profileList = searchService.search(requestParam);
        Map<String, Object> model = new HashMap<>();
        model.put("profileList", profileList);
        model.put("includeMid", "search.jsp");

        return new ModelAndView("profile", model);
    }

    @RequestMapping(path = "/search", method = RequestMethod.POST)
    protected ModelAndView postSearch(@RequestParam Map<String, String> requestParam) {

        Map<String, Object> model = new HashMap<>();
        Map<String, String> searchParamMap = new HashMap<>();
        String searchQuery = requestParam.get("searchQuery");
        searchParamMap.put("searchQuery", searchQuery);
        List<Profile> profileList = searchService.search(searchParamMap);
        model.put("profileList", convertProfileListToJson(profileList));
        model.put("includeMid", "search.jsp");

        return new ModelAndView("profile", model);
    }

    @RequestMapping(path = "/searchResult", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<ProfileDto> getSearchResult(@RequestParam Map<String, String> requestParam) {
        List<Profile> profileList = searchService.search(requestParam);

        return convertProfileListToJson(profileList);
    }

    private List<ProfileDto> convertProfileListToJson(List<Profile> profileList) {
        List<ProfileDto> jsonList = new LinkedList<>();

        for (Profile profile : profileList) {
            jsonList.add(new ProfileDto(profile));
        }

        return jsonList;
    }
}