package com.getjavajob.webapp.brikezn.spring.web.controller;

import com.getjavajob.webapp.brikezn.jpa.bean.location.City;
import com.getjavajob.webapp.brikezn.jpa.bean.location.Country;
import com.getjavajob.webapp.brikezn.jpa.bean.location.Region;
import com.getjavajob.webapp.brikezn.dto_bean.CityDto;
import com.getjavajob.webapp.brikezn.dto_bean.CountryDto;
import com.getjavajob.webapp.brikezn.dto_bean.RegionDto;
import com.getjavajob.webapp.brikezn.service.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
public class LocationController {

    @Autowired
    LocationService locationService;

    public LocationController() {

    }

    protected LocationController(LocationService locationService) {
        this.locationService = locationService;
    }

    @RequestMapping(path = "/getCountries", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<CountryDto> getCountries() {

        List<Country> countries = locationService.getAllCountries();
        List<CountryDto> json = new ArrayList<>();

        for (Country country : countries) {
            json.add(new CountryDto(country));
        }

        return json;
    }

    @RequestMapping(path = "/getRegions/{countryId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<RegionDto> getRegions(@PathVariable String countryId) {

        Country country = locationService.getCountry(Integer.parseInt(countryId));
        List<Region> regions = locationService.getRegions(country);
        List<RegionDto> json = new ArrayList<>();

        for (Region region : regions) {
            json.add(new RegionDto(region));
        }

        return json;
    }

    @RequestMapping(path = "/getCitiesByRegion/{regionId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<CityDto> getCitiesByRegion(@PathVariable String regionId) {

        Region region = locationService.getRegion(Integer.parseInt(regionId));
        List<City> cities = locationService.getCities(region);
        List<CityDto> json = new ArrayList<>();

        for (City city : cities) {
            json.add(new CityDto(city));
        }

        return json;
    }

    @RequestMapping(path = "/getCitiesByCountry/{countryId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<CityDto> getCities(@PathVariable String countryId) {

        Country country = locationService.getCountry(Integer.parseInt(countryId));
        List<City> cities = locationService.getCities(country);
        List<CityDto> json = new ArrayList<>();

        for (City city : cities) {
            json.add(new CityDto(city));
        }

        return json;
    }
}