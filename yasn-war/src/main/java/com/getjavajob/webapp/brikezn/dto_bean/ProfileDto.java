package com.getjavajob.webapp.brikezn.dto_bean;

import com.getjavajob.webapp.brikezn.jpa.bean.Avatar;
import com.getjavajob.webapp.brikezn.jpa.bean.Profile;
import com.getjavajob.webapp.brikezn.jpa.bean.location.City;
import com.getjavajob.webapp.brikezn.jpa.bean.location.Country;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class ProfileDto {

    private Integer id;
    private String email;
    private String firstName;
    private String lastName;
    private String nickname;
    private String gender;
    private String country;
    private String city;
    private Date birthday;
    private String avatarHtmlValue;

    public ProfileDto() {

    }

    public ProfileDto(Integer id, String email, String firstName, String lastName) {
        this.id = id;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public ProfileDto(Profile profile) {
        this.id = profile.getId();
        this.email = profile.getEmail();
        this.firstName = profile.getFirstName();
        this.lastName = profile.getLastName();
        this.nickname = profile.getNickname();
        this.gender = profile.getGender();
        this.birthday = profile.getBirthday();

        City city = profile.getCity();

        if (city != null) {
            this.city = profile.getCity().getTitle();
            Country country = city.getCountry();

            if (country != null) {
                this.country = country.getTitle();
            }
        }

        Avatar avatar = profile.getAvatar();

        if (avatar != null) {
            this.avatarHtmlValue = avatar.getHtmlValue();
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getAvatarHtmlValue() {
        return avatarHtmlValue;
    }

    public void setAvatarHtmlValue(String avatarHtmlValue) {
        this.avatarHtmlValue = avatarHtmlValue;
    }

    public static Set<ProfileDto> convertToJson(Set<Profile> profiles) {

        Set<ProfileDto> profileDtoSet = new HashSet<>();

        for (Profile profile : profiles) {
            ProfileDto profileDto = new ProfileDto(profile);
            profileDtoSet.add(profileDto);
        }

        return profileDtoSet;
    }
}