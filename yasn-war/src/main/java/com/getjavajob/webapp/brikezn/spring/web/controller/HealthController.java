package com.getjavajob.webapp.brikezn.spring.web.controller;

import com.getjavajob.webapp.brikezn.service.ProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

@Controller
public class HealthController {

    @Autowired
    ProfileService profileService;

    @RequestMapping(path = "/health", method = RequestMethod.GET)
    private ModelAndView testPostPhoto(@RequestParam("image") MultipartFile image) {

        Map<String, Object> model = new HashMap<>();
        int numberOfProfiles = profileService.getAll().size();
        model.put("profileCount", numberOfProfiles);

        return new ModelAndView("health", model);
    }
}