package com.getjavajob.webapp.brikezn;

import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class AuthFilter implements Filter {

    private static final Logger LOGGER = Logger.getLogger(AuthFilter.class);

    @Override
    public void init(FilterConfig filterConfig) {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) {

        final HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        response.setContentType("text/html;charset=UTF-8");

        if (httpServletRequest.getServletPath().contains("static")) {
            try {
                chain.doFilter(request, response);
            } catch (IOException e) {
                LOGGER.error(e);
            } catch (ServletException e) {
                LOGGER.error(e);
            }
        }

        final HttpSession session = httpServletRequest.getSession();
        final String id = String.valueOf(session.getAttribute("id"));
        final String servletPath = httpServletRequest.getServletPath();
        final String contextPath = httpServletRequest.getContextPath();

        if (servletPath.endsWith("css") || servletPath.endsWith("jpg")) {
            try {
                chain.doFilter(request, response);
            } catch (IOException e) {
                LOGGER.error(e);
            } catch (ServletException e) {
                LOGGER.error(e);
            }
        }

        if (id == null || id.equals("null")) {
            if (!("/login".equals(servletPath) || "/registr".equals(servletPath) || "/login.jsp".equals(servletPath) || "/registr.jsp".equals(servletPath) || "/user".equals(servletPath))) {
                if (session.getAttribute("path") == null && !"/style.css".equals(servletPath)) {
                    session.setAttribute("path", contextPath + servletPath);
                }

                RequestDispatcher requestDispatcher = httpServletRequest.getRequestDispatcher("/login");

                try {
                    requestDispatcher.forward(request, response);
                    httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + "/login");
                } catch (ServletException e) {
                    LOGGER.error(e);
                } catch (IOException e) {
                    LOGGER.error(e);
                }

                return;
            }
        }

        try {
            chain.doFilter(request, response);
        } catch (IOException e) {
            LOGGER.error(e);
        } catch (ServletException e) {
            LOGGER.error(e);
        }
    }

    @Override
    public void destroy() {

    }
}