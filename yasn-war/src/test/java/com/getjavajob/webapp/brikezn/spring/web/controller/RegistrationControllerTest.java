package com.getjavajob.webapp.brikezn.spring.web.controller;

import com.getjavajob.webapp.brikezn.dto_bean.ProfileDto;
import com.getjavajob.webapp.brikezn.jpa.bean.Profile;
import com.getjavajob.webapp.brikezn.jpa.bean.location.Country;
import com.getjavajob.webapp.brikezn.service.LocationService;
import com.getjavajob.webapp.brikezn.service.ProfileService;
import com.getjavajob.webapp.brikezn.service.SettingService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by Sierra.
 */
public class RegistrationControllerTest {

    ProfileService profileService;

    LocationService locationService;

    SettingService settingService;

    ProfileDto profile;


    @Before
    public void setup() {
        profileService = mock(ProfileService.class);
        locationService = mock(LocationService.class);
        settingService = mock(SettingService.class);
        Profile lisztProfile = new Profile("list@gmail.com", "pass", "Ferenc", "Liszt", "m",
                new Date(System.currentTimeMillis()));
        lisztProfile.setId(1);
        profile = new ProfileDto(lisztProfile);

        when(locationService.getAllCountries()).thenReturn(new ArrayList<Country>());
        when(profileService.get("registeredEmail")).thenReturn(new Profile());
    }

    private RegistrationController createRegistrationController() {
        return new RegistrationController(profileService, locationService, settingService, profile);
    }

    @Test
    public void getRegistration_must_return_model_and_view_with_registration_page_parameters() {
        // creating controller
        RegistrationController controller = createRegistrationController();

        // when getRegistration() is called
        ModelAndView modelAndView = controller.getRegistration();
        String resultView = modelAndView.getViewName();

        // then return model and view with registration page parameters
        assertEquals("registr", resultView);
    }

    @Test
    public void postRegistration_must_return_redirect_to_profile_page() {
        // creating controller and preparing registration parameters map with correct values
        RegistrationController controller = createRegistrationController();
        Map<String, Object> registrationParameters = new HashMap<>();
        registrationParameters.put("email", "list@gmail.com");
        registrationParameters.put("password", "pass");
        registrationParameters.put("confirmPassword", "pass");

        // when postRegistration() with correct parameters is called
        ModelAndView modelAndView = controller.postRegistration(registrationParameters, new MockHttpSession());

        // then returning model and view with redirect to profile page
        assertEquals("redirect:/profile", modelAndView.getViewName());
    }

    @Test
    public void postRegistration_with_incorrect_registration_parameters_must_return_registration_page_and_error_map() {
        // creating controller and preparing registration parameters map with already registred email and
        // not confirmed password
        RegistrationController controller = createRegistrationController();
        Map<String, Object> registrationParameters = new HashMap<>();
        registrationParameters.put("email", "registeredEmail");
        registrationParameters.put("password", "pass");
        registrationParameters.put("confirmPassword", "incorrectly_confirmed_pass");

        // when postRegistration with incorrect email and password parameters is called
        ModelAndView modelAndView = controller.postRegistration(registrationParameters, new MockHttpSession());
        Map<String, Object> model = modelAndView.getModel();
        final Object registrationErrorMap = model.get("regErrorMap");
        final String viewName = modelAndView.getViewName();

        // then returning registration view and not null map of registration errors
        assertEquals("registr", viewName);
        assertNotNull(registrationErrorMap);
    }

    @Test
    public void getErrors_must_return_email_error() {
        // creating controller and preparing registration param map with already registered email
        RegistrationController controller = createRegistrationController();
        Map<String, Object> registrationParameters = new HashMap<>();
        registrationParameters.put("email", "registeredEmail");
        registrationParameters.put("password", "pass");
        registrationParameters.put("confirmPassword", "pass");

        // when getErrors() with already registered email and correct password is called
        Map<String, String> errors = controller.getErrors(registrationParameters);
        final String emailError = errors.get("emailError");

        // then returning registration error map with only one error (incorrect email)
        assertEquals(1, errors.size());
        assertEquals("email already exists", emailError);
    }

    @Test
    public void getErrors_must_return_password_error() {
        // creating controller and preparing registration param map with incorrect password
        RegistrationController controller = createRegistrationController();
        Map<String, Object> registrationParameters = new HashMap<>();
        registrationParameters.put("email", "nptRegisteredEmail");
        registrationParameters.put("password", "pass");
        registrationParameters.put("confirmPassword", "incorrectly_confirmed_pass");

        // when getErrors() with correct email and incorrect password is called
        Map<String, String> errors = controller.getErrors(registrationParameters);
        final String passwordError = errors.get("confirmPasswordError");

        // then returning registration error map with only one error (incorrect email)
        assertEquals(1, errors.size());
        assertEquals("password does not confirmed", passwordError);
    }
}
