package com.getjavajob.webapp.brikezn.spring.web.controller;

import com.getjavajob.webapp.brikezn.dto_bean.ProfileDto;
import com.getjavajob.webapp.brikezn.jpa.bean.Profile;
import com.getjavajob.webapp.brikezn.service.ProfileService;
import com.getjavajob.webapp.brikezn.service.RelationshipService;
import com.getjavajob.webapp.brikezn.service.SettingService;
import com.getjavajob.webapp.brikezn.service.WallService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by Sierra.
 */
public class ProfileControllerTest {

    private ProfileService profileService;

    private SettingService settingService;

    private WallService wallService;

    private RelationshipService relationshipService;

    private ProfileDto profile;

    @Before
    public void setup() {
        profileService = mock(ProfileService.class);
        settingService = mock(SettingService.class);
        wallService = mock(WallService.class);
        relationshipService = mock(RelationshipService.class);
        Profile profileLiszt = new Profile("list@gmail.com", "pass", "Ferenc", "Liszt", "m",
                new Date(System.currentTimeMillis()));
        profile = new ProfileDto(profileLiszt);
        profile.setId(1);
        Map<String, Boolean> accessParamMap = new HashMap<>();
        accessParamMap.put("wallView", true);

        when(profileService.get(1)).thenReturn(profileLiszt);
        when(settingService.getAccessParameters(1, 2)).thenReturn(accessParamMap);
        when(relationshipService.getFriends(1)).thenReturn(new HashSet<Profile>());
    }

    private ProfileController createProfileController(Boolean isLoggedUser) {
        return isLoggedUser ?
                new ProfileController(profileService, settingService, wallService, relationshipService, profile) :
                new ProfileController(profileService, settingService, wallService, relationshipService,
                        new ProfileDto());
    }

    private void assertEqualsProfileSettingsModelAndVieW(ModelAndView modelAndView) {
        // preparing expected values
        final String expectedIsLogged = "true";
        final String expectedIncludePage = "profileSettings.jsp";
        final String expectedViewName = "profile";

        // preparing result values
        Map<String, Object> model = modelAndView.getModel();
        final Object resultIsLogged = model.get("logged");
        final Object resultIncludeValue = model.get("includeMid");
        final Object resultViewName = modelAndView.getViewName();

        // assertion that expected values equal to result
        assertEquals(expectedIsLogged, resultIsLogged);
        assertEquals(expectedIncludePage, resultIncludeValue);
        assertEquals(expectedViewName, resultViewName);
    }

    @Test
    public void getProfile_not_null_profile_bean_id_returns_redirect_to_profile_page() {
        // creating profile controller
        ProfileController profileController = createProfileController(true);

        // when getProfile() with not null id of session dto profile bean is called
        String result = profileController.getProfile();

        // then returning redirect string to profile page
        assertEquals(result, "redirect:/profile/1");
    }

    @Test
    public void getProfile_null_profile_bean_id_returns_redirect_to_login_page() {
        // creating profile controller with null id of profileDto session bean
        ProfileController profileController = createProfileController(false);

        // when getProfile() with null id of session dto profile bean is called
        String result = profileController.getProfile();

        // then returning redirect string to login page
        assertEquals(result, "redirect:/login");
    }

    @Test
    public void getProfile_with_string_param_returns_redirect_to_login() {
        // creating new controller with null id session-scope ProfileDto
        ProfileController profileController = createProfileController(false);

        // when getProfile(String profileId) with null param and null id of session-scope profile bean is called
        ModelAndView modelAndView = profileController.getProfile(null);
        String result = modelAndView.getViewName();

        //then returns redirect to login page
        assertEquals(result, "redirect:/login");
    }

    @Test
    public void getProfile_with_string_param_returns_redirect_to_main_page() {
        // creating new controller with not null id session-scope ProfileDto
        ProfileController profileController = createProfileController(true);

        // when getProfile(String profileId) with null param and null id of session-scope profile bean is called
        ModelAndView modelAndView = profileController.getProfile(null);
        String result = modelAndView.getViewName();

        //then returns redirect to main page
        assertEquals(result, "redirect:/profile/1");
    }

    @Test
    public void getProfile_with_string_param_returns_redirect_to_target_page() {
        // creating new controller and preparing model test values
        ProfileController profileController = createProfileController(true);
        final String expectedView = "profile";
        final String expectedIncludePage = "profileInfo.jsp";

        // when getProfile(String profileId) is called
        ModelAndView modelAndView = profileController.getProfile("2");
        final String resultView = modelAndView.getViewName();
        final Object resultIncludePage = modelAndView.getModel().get("includeMid");

        //then returns view: "profile" and add-on page: "profileInfo.jsp"
        assertEquals(expectedView, resultView);
        assertEquals(expectedIncludePage, resultIncludePage);
    }

    @Test
    public void postProfile_must_return_redirect_to_profile_page() {
        // creating new controller
        ProfileController profileController = createProfileController(true);

        // when postProfile(String profileId, String wallMessage) is called
        String result = profileController.postProfile("2", "Wall message!");

        // then returning redirect to profile page which is target to post wall message
        assertEquals("redirect:/profile/2", result);
    }

    @Test
    public void settings_with_logged_user_must_return_model_and_view_for_profile_settings() {
        // creating new controller
        ProfileController profileController = createProfileController(true);

        // when settings() is called
        ModelAndView modelAndView = profileController.settings();

        // then returning ModelAndView with expected values for profile settings page
        assertEqualsProfileSettingsModelAndVieW(modelAndView);
    }

    @Test
    public void settings_with_not_logged_user_must_return_redirect_to_login_page() {
        // creating new controller with not logged user
        ProfileController profileController = createProfileController(false);

        // when settings() with not logged user is called
        ModelAndView modelAndView = profileController.settings();
        final String viewName = modelAndView.getViewName();

        // then returning redirect to login page
        assertEquals("redirect:/login", viewName);
    }

    @Test
    public void changeProfileImage_must_return_model_and_view_for_profile_settings() {
        // creating new controller and mock for multipart file
        ProfileController profileController = createProfileController(true);
        MockMultipartFile file = new MockMultipartFile("file", "content".getBytes());

        // when changeProfileImage(MultipartFile file) is called
        ModelAndView modelAndView = profileController.changeProfileImage(file);

        // then returning ModelAndView with expected values for profile settings page
        assertEqualsProfileSettingsModelAndVieW(modelAndView);
    }

    @Test
    public void settingUpdate_must_return_model_and_view_for_profile_settings() {
        // creating new controller
        ProfileController profileController = createProfileController(true);
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("settingAction", "security");

        // when settingUpdate(Map<String, Object> paramMap) is called
        ModelAndView modelAndView = profileController.settingsUpdate(paramMap);

        // then returning ModelAndView with expected values for profile settings page
        assertEqualsProfileSettingsModelAndVieW(modelAndView);
    }

    @Test
    public void getFriendList_with_logged_user_must_return_redirect_to_friend_list_page() {
        // creating new controller with logged user (session-scope profile bean id value is not null)
        ProfileController profileController = createProfileController(true);

        // when getFriendList is called
        String result = profileController.getFriendList();

        // then returning redirect to friend list page
        assertEquals("redirect:/friends/1", result);
    }

    @Test
    public void getFriendList_with_not_logged_user_must_redirect_to_login_page() {
        // creating new controller with not logged user (session-scope profile bean id value is null)
        ProfileController profileController = createProfileController(false);

        // when getFriendList is called
        String result = profileController.getFriendList();

        // then returning redirect to login page
        assertEquals("redirect:/login", result);
    }

    @Test
    public void getProfileFriendList_must_return_model_and_view_for_friend_list_page() {
        // creating new controller
        ProfileController profileController = createProfileController(true);

        // when getProfileFriendList(String profileId) is called
        ModelAndView modelAndView = profileController.getProfileFriendList("1");
        final String resultViewName = modelAndView.getViewName();
        Map<String, Object> model = modelAndView.getModel();
        final Object resultIncludeMid = model.get("includeMid");

        // then returns ModelAndView with params for friend list page
        assertEquals("../templates/profileList.jsp", resultIncludeMid);
        assertEquals("profile", resultViewName);
    }
}