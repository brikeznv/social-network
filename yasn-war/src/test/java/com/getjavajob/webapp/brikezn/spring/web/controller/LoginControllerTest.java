package com.getjavajob.webapp.brikezn.spring.web.controller;

import com.getjavajob.webapp.brikezn.jpa.bean.Profile;
import com.getjavajob.webapp.brikezn.service.ProfileService;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by Sierra.
 */
public class LoginControllerTest {

    ProfileService profileService;

    @Before
    public void setup() {
        profileService = mock(ProfileService.class);

        Profile profile = new Profile("list@gmail.com", "pass", "Ferenc", "Liszt", "m",
                new Date(System.currentTimeMillis()));

        when(profileService.get(1)).thenReturn(profile);
        when(profileService.get("list@gmail.com")).thenReturn(profile);
    }


    @Test
    public void loginGet_null_id_must_return_login_page() {
        // creating LoginController object
        LoginController loginController = new LoginController();

        // when login() with null "id" session attribute is called
        String result = loginController.login(null);

        // then returning login page
        assertEquals(result, "login");
    }

    @Test
    public void loginGet_notnull_id_must_return_redirect_profile() {
        // creating LoginController object
        LoginController loginController = new LoginController();

        // when login() with not null "id" session attribute is called
        String result = loginController.login(1);

        // then returning redirecting to profile page
        assertEquals(result, "redirect:/profile");
    }

    @Test
    public void getLoginErrors_with_correct_values_returning_empty_string() {
        // creating LoginController object and login param map
        LoginController loginController = new LoginController(profileService);
        Map<String, String> paramMap = new HashMap<>();
        paramMap.put("email", "list@gmail.com");
        paramMap.put("password", "pass");

        // when getLoginErrors with correct login parameters is called
        String error = loginController.getLoginError(paramMap);

        // then returning empty string
        assertEquals(error.length(), 0);
    }

    @Test
    public void getLoginErrors_with_incorrect_login_returning_login_error_string() {
        // creating LoginController object and login param map
        LoginController loginController = new LoginController(profileService);
        Map<String, String> paramMap = new HashMap<>();
        paramMap.put("email", "incorrect_login");
        paramMap.put("password", "pass");

        // when getLoginErrors with incorrect email is called
        String error = loginController.getLoginError(paramMap);

        // then returning email error message
        assertEquals(error, "No such email!");
    }

    @Test
    public void getLoginErrors_with_incorrect_password_returning_password_error_string() {
        // creating LoginController object and login param map
        LoginController loginController = new LoginController(profileService);
        Map<String, String> paramMap = new HashMap<>();
        paramMap.put("email", "list@gmail.com");
        paramMap.put("password", "incorrect_password");

        // when getLoginErrors with incorrect email is called
        String error = loginController.getLoginError(paramMap);

        // then returning email error message
        assertEquals(error, "Incorrect password!");
    }
}