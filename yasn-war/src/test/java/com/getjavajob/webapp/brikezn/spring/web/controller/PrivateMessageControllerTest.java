package com.getjavajob.webapp.brikezn.spring.web.controller;

import com.getjavajob.webapp.brikezn.dto_bean.PrivateMessageDto;
import com.getjavajob.webapp.brikezn.dto_bean.ProfileDto;
import com.getjavajob.webapp.brikezn.jpa.bean.PrivateMessage;
import com.getjavajob.webapp.brikezn.jpa.bean.Profile;
import com.getjavajob.webapp.brikezn.service.PrivateMessageService;
import com.getjavajob.webapp.brikezn.service.ProfileService;
import com.getjavajob.webapp.brikezn.service.RelationshipService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.web.servlet.ModelAndView;

import java.math.BigInteger;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by Sierra.
 */
public class PrivateMessageControllerTest {

    PrivateMessageService privateMessageService;

    ProfileService profileService;

    ProfileDto profile;

    RelationshipService relationshipService;

    @Before
    public void setup() {
        privateMessageService = mock(PrivateMessageService.class);
        profileService = mock(ProfileService.class);
        relationshipService = mock(RelationshipService.class);
        profile = new ProfileDto();
        profile.setId(1);
        Profile profileLiszt = new Profile("list@gmail.com", "pass", "Ferenc", "Liszt", "m",
                new Date(System.currentTimeMillis()));
        Profile profileEinshtein = new Profile("einshtein@gmail.com", "pass", "Albert", "Einshtein", "m",
                new Date(System.currentTimeMillis()));
        Set<Profile> friends = new HashSet<>();
        friends.add(profileEinshtein);
        List<PrivateMessage> messages = new LinkedList<>();
        messages.add(new PrivateMessage(profileLiszt, profileEinshtein, "test msg!", new BigInteger("123")));

        when(profileService.get(1)).thenReturn(profileLiszt);
        when(profileService.get(2)).thenReturn(profileEinshtein);
        when(relationshipService.getFriends(1)).thenReturn(friends);
        when(privateMessageService.get(1, 2)).thenReturn(messages);
        when(privateMessageService.get(1, 2, 1)).thenReturn(messages);
    }

    @Test
    public void getPrivateMessages_with_null_member_returning_ModelAndView_with_dialogue_list() {
        // creating controller
        PrivateMessagesController controller = new PrivateMessagesController(privateMessageService, profileService,
                relationshipService, profile);

        // when getPrivateMessages(String member) with null member is called
        ModelAndView returnValue = controller.getPrivateMessages(null);

        // then returning ModelAndView with "profile" main jsp page and dialogue page params
        Map<String, Object> model = returnValue.getModel();
        Object includeMid = model.get("includeMid");
        Set<Profile> friends = (Set<Profile>) model.get("friendList");
        assertEquals(returnValue.getViewName(), "profile");
        assertEquals(includeMid, "../templates/dialogues.jsp");
        assertEquals(friends.size(), 1);
    }

    @Test
    public void getPrivateMessages_with_not_null_member_returning_ModelAndView_with_private_messages_page() {
        // creating controller
        PrivateMessagesController controller = new PrivateMessagesController(privateMessageService, profileService,
                relationshipService, profile);

        // when getPrivateMessages(String member) with null member is called
        ModelAndView returnValue = controller.getPrivateMessages("2");

        // then returning ModelAndView with "profile" main jsp page and private messages params
        assertEquals(returnValue.getViewName(), "profile");

        Map<String, Object> model = returnValue.getModel();
        Object includeMid = model.get("includeMid");
        assertEquals(includeMid, "../templates/privateMessageList.jsp");

        List<PrivateMessage> privateMessageList = (List<PrivateMessage>) model.get("privateMessageList");
        assertEquals(privateMessageList.size(), 1);

        int countOfPages = (int) model.get("countOfPages");
        assertEquals(countOfPages, 1);

        ProfileDto mainProfile = (ProfileDto) model.get("profile");
        assertEquals(mainProfile.getFirstName(), "Ferenc");

        ProfileDto memberProfile = (ProfileDto) model.get("memberProfile");
        assertEquals(memberProfile.getFirstName(), "Albert");
    }

    @Test
    public void getDialogues_returning_ModelAndView_with_dialogue_list() {
        // creating controller
        PrivateMessagesController controller = new PrivateMessagesController(privateMessageService, profileService,
                relationshipService, profile);

        // when getDialogues() is called
        ModelAndView returnValue = controller.getDialogues();

        // then returning ModelAndView with "profile" view, dialogues list as add-on page, and friend list
        Map<String, Object> model = returnValue.getModel();
        Object includeMid = model.get("includeMid");
        Set<Profile> friends = (Set<Profile>) model.get("friendList");
        assertEquals(returnValue.getViewName(), "profile");
        assertEquals(includeMid, "../templates/dialogues.jsp");
        assertEquals(friends.size(), 1);
    }

    @Test
    public void postPrivateMessage_returning_redirect_to_dialogue_with_member() {
        // creating controller and request param map
        PrivateMessagesController controller = new PrivateMessagesController(privateMessageService, profileService,
                relationshipService, profile);
        HashMap<String, Object> reqParam = new HashMap<String, Object>();
        reqParam.put("member", "2");

        // when postPrivateMessage(Map<String, Object> reqParam) is called
        String returnValue = controller.postPrivateMessage(reqParam);

        //then returning redirect to dialogue with member
        assertEquals(returnValue, "redirect:/dialogues?member=2");
    }

    @Test
    public void getSearchResult_returning_private_message_dtos_list() {
        // creating controller and method parameters
        PrivateMessagesController controller = new PrivateMessagesController(privateMessageService, profileService,
                relationshipService, profile);
        String member = "2";
        String page = "1";

        // when getSearchResult(String member, String page) is called
        List<PrivateMessageDto> result = controller.getSearchResult(member, page);

        // then returning List<PrivateMessageDto>
        assertEquals(result.size(), 1);
    }
}