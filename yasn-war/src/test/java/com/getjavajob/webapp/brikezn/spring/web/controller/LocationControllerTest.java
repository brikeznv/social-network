package com.getjavajob.webapp.brikezn.spring.web.controller;

import com.getjavajob.webapp.brikezn.dto_bean.CityDto;
import com.getjavajob.webapp.brikezn.dto_bean.CountryDto;
import com.getjavajob.webapp.brikezn.dto_bean.RegionDto;
import com.getjavajob.webapp.brikezn.jpa.bean.location.City;
import com.getjavajob.webapp.brikezn.jpa.bean.location.Country;
import com.getjavajob.webapp.brikezn.jpa.bean.location.Region;
import com.getjavajob.webapp.brikezn.service.LocationService;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class LocationControllerTest {

    LocationService locationService;

    final Country mockCountry = new Country();

    final Region mockRegion = new Region();

    @Before
    public void setup() {
        locationService = mock(LocationService.class);
        when(locationService.getRegion(1)).thenReturn(mockRegion);
        when(locationService.getCountry(1)).thenReturn(mockCountry);

        ArrayList<Country> countries = new ArrayList<>();
        countries.add(new Country(1, "Russia"));
        countries.add(new Country(2, "Finland"));
        when(locationService.getAllCountries()).thenReturn(countries);

        ArrayList<Region> regions = new ArrayList<>();
        regions.add(new Region(1, mockCountry, "Leningradskaya oblast"));
        when(locationService.getRegions(mockCountry)).thenReturn(regions);

        ArrayList<City> cities = new ArrayList<>();
        cities.add(new City(1, "Saint-Petersburg"));
        when(locationService.getCities(mockCountry)).thenReturn(cities);
        when(locationService.getCities(mockRegion)).thenReturn(cities);
    }

    @Test
    public void getCountries_must_return_list_of_countries() {
        // creating controller
        LocationController locationController = new LocationController(locationService);

        // when method getCountries is called
        List<CountryDto> countryList = locationController.getCountries();

        // then returning list of country DTOs
        assertEquals(2, countryList.size());
    }

    @Test
    public void getRegions_must_return_list_of_regions() {
        // creating controller
        LocationController locationController = new LocationController(locationService);

        // when getRegions is called
        List<RegionDto> regions = locationController.getRegions("1");

        // then returning list of region DTOs
        assertEquals(1, regions.size());
    }

    @Test
    public void getCitiesByRegion_must_return_list_of_cities() {
        // creating controller
        LocationController locationController = new LocationController(locationService);

        // when getCitiesByRegion is called
        List<CityDto> cities = locationController.getCitiesByRegion("1");

        // then returning list of city DTOs
        assertEquals(1, cities.size());
    }

    @Test
    public void getCities_must_return_list_of_cities_by_country() {
        // creating controller
        LocationController locationController = new LocationController(locationService);

        // when getCities is called
        List<CityDto> cities = locationController.getCities("1");

        // then returning list of city DTOs
        assertEquals(1, cities.size());
    }
}