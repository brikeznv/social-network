package com.getjavajob.webapp.brikezn.spring.web.controller;

import com.getjavajob.webapp.brikezn.dto_bean.ProfileDto;
import com.getjavajob.webapp.brikezn.jpa.bean.Profile;
import com.getjavajob.webapp.brikezn.service.ProfileService;
import com.getjavajob.webapp.brikezn.service.SearchService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.web.servlet.ModelAndView;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by Sierra.
 */
public class SearchControllerTest {

    SearchService searchService;

    ProfileService profileService;

    Map<String, String> searchParameters;

    @Before
    public void setup() {
        searchService = mock(SearchService.class);
        profileService = mock(ProfileService.class);
        searchParameters = new HashMap<>();
        Profile profileLiszt = new Profile("list@gmail.com", "pass", "Ferenc", "Liszt", "m",
                new Date(System.currentTimeMillis()));
        List<Profile> searchResult = new LinkedList<>();
        searchResult.add(profileLiszt);

        when(searchService.search(searchParameters)).thenReturn(searchResult);
    }

    private SearchController createSearchController() {
        return new SearchController(searchService, profileService);
    }

    private void assertEqualSearchPageParameters(ModelAndView modelAndView) {
        // preparing result values
        Map<String, Object> model = modelAndView.getModel();
        final Object includePage = model.get("includeMid");
        final String viewName = modelAndView.getViewName();

        // assertion that expected values equal to result
        assertEquals("search.jsp", includePage);
        assertEquals("profile", viewName);
    }

    @Test
    public void getSearch_must_return_model_and_view_with_search_page_parameters() {
        // creating controller
        SearchController controller = createSearchController();

        // when getSearch is called
        ModelAndView modelAndView = controller.getSearch(searchParameters);

        // then returning ModelAndView for search page
        assertEqualSearchPageParameters(modelAndView);
    }

    @Test
    public void postSearch_must_return_model_and_view_with_search_page_parameters() {
        // creating controller
        SearchController controller = createSearchController();

        // then postSearch is called
        ModelAndView modelAndView = controller.postSearch(searchParameters);

        // then returning ModelAndView for search page
        assertEqualSearchPageParameters(modelAndView);
    }

    @Test
    public void getSearchResult_must_return_list_of_profile_dto() {
        // creating controller
        SearchController controller = createSearchController();

        // when getSearchResult() is called
        List<ProfileDto> result = controller.getSearchResult(searchParameters);
        ProfileDto profileLisztDto = result.get(0);

        // then returning list of profiles dto
        assertEquals("list@gmail.com", profileLisztDto.getEmail());
    }
}
