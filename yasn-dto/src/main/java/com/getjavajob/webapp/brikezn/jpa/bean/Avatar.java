package com.getjavajob.webapp.brikezn.jpa.bean;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "avatar")
public class Avatar implements Serializable {

    @Id
    @OneToOne
    @JoinColumn(name = "avatar_profile_id")
    private Profile owner;

    @OneToOne
    @JoinColumn(name = "avatar_img_id")
    private Image image;

    public Avatar() {

    }

    public Avatar(Profile owner, Image image) {
        this.owner = owner;
        this.image = image;
    }

    public Profile getOwner() {
        return owner;
    }

    public void setOwner(Profile owner) {
        this.owner = owner;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public byte[] getBytes() {
        return image.getBytes();
    }

    public String getHtmlValue() {
        byte[] bytes = image.getBytes();
        String encoded = Base64.encode(bytes);
        return "data:image/jpg; base64," + encoded;
    }
}