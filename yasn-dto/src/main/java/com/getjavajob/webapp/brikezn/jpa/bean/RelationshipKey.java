package com.getjavajob.webapp.brikezn.jpa.bean;

import com.getjavajob.webapp.brikezn.jpa.bean.Profile;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Embeddable
public class RelationshipKey implements Serializable {

    @ManyToOne
    @JoinColumn(name = "relationship_profile_object")
    private Profile object;

    @ManyToOne
    @JoinColumn(name = "relationship_profile_subject")
    private Profile subject;

    public RelationshipKey() {

    }

    public RelationshipKey(Profile object, Profile subject) {
        this.object = object;
        this.subject = subject;
    }

    public Profile getObject() {
        return object;
    }

    public void setObject(Profile object) {
        this.object = object;
    }

    public Profile getSubject() {
        return subject;
    }

    public void setSubject(Profile subject) {
        this.subject = subject;
    }
}