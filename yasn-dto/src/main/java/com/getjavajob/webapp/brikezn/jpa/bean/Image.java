package com.getjavajob.webapp.brikezn.jpa.bean;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "img")
public class Image implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "img_id")
    private Integer id;

    @Column(name = "img_bytes")
    private byte[] bytes;

    public Image() {

    }

    public Image(byte[] bytes) {
        this.bytes = bytes;
    }

    public Image(Integer id, byte[] bytes) {
        this.id = id;
        this.bytes = bytes;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public byte[] getBytes() {
        return bytes;
    }

    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }
}