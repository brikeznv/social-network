package com.getjavajob.webapp.brikezn.jpa.bean;

import com.getjavajob.webapp.brikezn.jpa.bean.Profile;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Embeddable
public class WallMessageKey implements Serializable {

    @ManyToOne
    @JoinColumn(name = "wall_sender")
    private Profile sender;

    @ManyToOne
    @JoinColumn(name = "wall_recipient")
    private Profile recipient;

    @Column(name = "wall_date")
    private Long date;

    public WallMessageKey() {

    }

    public WallMessageKey(Profile sender, Profile recipient, Long date) {
        this.sender = sender;
        this.recipient = recipient;
        this.date = date;
    }

    public Profile getSender() {
        return sender;
    }

    public void setSender(Profile sender) {
        this.sender = sender;
    }

    public Profile getRecipient() {
        return recipient;
    }

    public void setRecipient(Profile recipient) {
        this.recipient = recipient;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }
}