package com.getjavajob.webapp.brikezn.jpa.bean.location;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "region")
public class Region implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "region_id")
    private Integer id;

    @OneToOne
    @JoinColumn(name = "country_id")
    private Country country;

    @Column(name = "title")
    private String title;

    public Region() {

    }

    public Region(Integer id, Country country, String title) {
        this.id = id;
        this.country = country;
        this.title = title;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
