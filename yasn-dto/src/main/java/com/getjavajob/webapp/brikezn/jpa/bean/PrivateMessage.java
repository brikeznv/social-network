package com.getjavajob.webapp.brikezn.jpa.bean;

import javax.persistence.*;
import java.math.BigInteger;

@Entity
@Table(name = "private_msg")
public class PrivateMessage {

    @Id
    @Column(name = "msg_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private BigInteger id;

    @ManyToOne
    @JoinColumn(name = "msg_sender_id")
    private Profile sender;

    @ManyToOne
    @JoinColumn(name = "msg_recipient_id")
    private Profile recipient;

    @Column(name = "msg_content")
    private String content;

    @Column(name = "msg_date")
    private BigInteger date;

    public PrivateMessage() {

    }

    public PrivateMessage(Profile sender, Profile recipient, String content, BigInteger date) {
        this.sender = sender;
        this.recipient = recipient;
        this.content = content;
        this.date = date;
    }

    public Profile getSender() {
        return sender;
    }

    public void setSender(Profile sender) {
        this.sender = sender;
    }

    public Profile getRecipient() {
        return recipient;
    }

    public void setRecipient(Profile recipient) {
        this.recipient = recipient;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public BigInteger getDate() {
        return date;
    }

    public void setDate(BigInteger date) {
        this.date = date;
    }

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

}
