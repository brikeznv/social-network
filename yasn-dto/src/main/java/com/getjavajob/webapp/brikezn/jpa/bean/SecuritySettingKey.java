package com.getjavajob.webapp.brikezn.jpa.bean;

import com.getjavajob.webapp.brikezn.jpa.bean.Profile;
import com.getjavajob.webapp.brikezn.jpa.bean.SecurityType;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import java.io.Serializable;

@Embeddable
public class SecuritySettingKey implements Serializable {

    @OneToOne
    @JoinColumn(name = "security_profile_id")
    private Profile owner;

    @OneToOne
    @JoinColumn(name = "security_profile_type")
    private SecurityType securityType;

    public SecuritySettingKey() {

    }

    public Profile getOwner() {
        return owner;
    }

    public void setOwner(Profile owner) {
        this.owner = owner;
    }

    public SecurityType getSecurityType() {
        return securityType;
    }

    public void setSecurityType(SecurityType securityType) {
        this.securityType = securityType;
    }

    public SecuritySettingKey(Profile owner, SecurityType securityType) {

        this.owner = owner;
        this.securityType = securityType;
    }
}