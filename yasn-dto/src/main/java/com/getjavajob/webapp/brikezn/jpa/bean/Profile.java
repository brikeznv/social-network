package com.getjavajob.webapp.brikezn.jpa.bean;

import com.getjavajob.webapp.brikezn.jpa.bean.location.City;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "profile")
public class Profile implements Serializable {

    @Id
    @Column(name = "profile_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "profile_email")
    private String email;

    @Column(name = "profile_password")
    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(name = "profile_first_name")
    private String firstName;

    @Column(name = "profile_last_name")
    private String lastName;

    @Column(name = "profile_nickname")
    private String nickname;

    @Column(name = "profile_gender")
    private String gender;

    @OneToOne
    @JoinColumn(name = "profile_city")
    private City city;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "profile_birthday")
    private Date birthday;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    @JoinColumn(name = "relationship_profile_object", referencedColumnName = "profile_id")
    private Set<ProfileRelationship> relationshipList;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    @JoinColumn(name = "security_profile_id")
    private List<SecuritySetting> securitySettingList;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    @JoinColumn(name = "wall_recipient")
    private List<WallMessage> wallMessageList;

    @OneToOne
    @JoinColumn(name = "profile_id")
    private Avatar avatar;

    public Profile() {

    }

    public Profile(String email, String password, String firstName, String lastName, String gender, Date birthday) {
        this.email = email;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.birthday = birthday;
    }

    public Avatar getAvatar() {
        return avatar;
    }

    public void setAvatar(Avatar avatar) {
        this.avatar = avatar;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Set<ProfileRelationship> getRelationshipList() {
        return relationshipList;
    }

    public void setRelationshipList(Set<ProfileRelationship> relationshipList) {
        this.relationshipList = relationshipList;
    }

    public List<SecuritySetting> getSecuritySettingList() {
        return securitySettingList;
    }

    public void setSecuritySettingList(List<SecuritySetting> securitySettingList) {
        this.securitySettingList = securitySettingList;
    }

    public List<WallMessage> getWallMessageList() {
        return wallMessageList;
    }

    public void setWallMessageList(List<WallMessage> wallMessageList) {
        this.wallMessageList = wallMessageList;
    }

    public Set<Profile> getFriends() {

        Set<Profile> friendList = new HashSet<>();

        for (ProfileRelationship rel : relationshipList) {

            Profile profile = rel.getId().getSubject();

            if ("friend".equals(rel.getRelationshipValue().getValue())) {
                friendList.add(profile);
            }
        }

        return friendList;
    }
}