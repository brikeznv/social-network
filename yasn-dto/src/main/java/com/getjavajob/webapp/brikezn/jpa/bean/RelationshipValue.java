package com.getjavajob.webapp.brikezn.jpa.bean;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "relationship")
public class RelationshipValue implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "relationship_id")
    private Integer id;

    @Column(name = "relationship_value")
    private String value;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}