package com.getjavajob.webapp.brikezn.jpa.bean;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

@Entity
@Table(name = "wall")
public class WallMessage implements Serializable {

    @Id
    @Column(name = "wall_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "wall_sender")
    private Profile sender;

    @ManyToOne
    @JoinColumn(name = "wall_recipient")
    private Profile recipient;

    @Column(name = "wall_content")
    private String content;

    @Column(name = "wall_date")
    private BigInteger date;

    public WallMessage() {

    }

    public WallMessage(Profile sender, Profile recipient, String content, Date date) {
        this.sender = sender;
        this.recipient = recipient;
        this.content = content;
        String postTime = String.valueOf(date.getTime());
        this.date = new BigInteger(postTime);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Profile getSender() {
        return sender;
    }

    public void setSender(Profile sender) {
        this.sender = sender;
    }

    public Profile getRecipient() {
        return recipient;
    }

    public void setRecipient(Profile recipient) {
        this.recipient = recipient;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public BigInteger getDate() {
        return date;
    }

    public void setDate(BigInteger date) {
        this.date = date;
    }
}