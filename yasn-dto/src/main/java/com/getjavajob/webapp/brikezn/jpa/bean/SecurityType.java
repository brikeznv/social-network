package com.getjavajob.webapp.brikezn.jpa.bean;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "security_type")
public class SecurityType implements Serializable {

    @Id
    @Column(name = "security_type_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "security_type_name")
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}