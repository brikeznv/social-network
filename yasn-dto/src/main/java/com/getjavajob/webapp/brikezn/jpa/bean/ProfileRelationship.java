package com.getjavajob.webapp.brikezn.jpa.bean;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "relationship_profile")
public class ProfileRelationship implements Serializable {

    @EmbeddedId
    private RelationshipKey id;

    @OneToOne
    @JoinColumn(name = "relationship_profile_value")
    private RelationshipValue relationshipValue;

    public ProfileRelationship() {

    }

    public ProfileRelationship(RelationshipKey id, RelationshipValue relationshipValue) {
        this.id = id;
        this.relationshipValue = relationshipValue;
    }

    public RelationshipKey getId() {
        return id;
    }

    public void setId(RelationshipKey id) {
        this.id = id;
    }

    public RelationshipValue getRelationshipValue() {
        return relationshipValue;
    }

    public void setRelationshipValue(RelationshipValue relationshipValue) {
        this.relationshipValue = relationshipValue;
    }
}