package com.getjavajob.webapp.brikezn.jpa.bean;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "security_profile")
public class SecuritySetting implements Serializable {

    @EmbeddedId
    private SecuritySettingKey id;

    @OneToOne
    @JoinColumn(name = "security_profile_value")
    private SecurityValue value;

    public SecuritySetting() {

    }

    public SecuritySetting(SecuritySettingKey id) {
        this.id = id;
    }

    public SecuritySetting(SecuritySettingKey id, SecurityValue value) {
        this.id = id;
        this.value = value;
    }

    public SecuritySettingKey getId() {
        return id;
    }

    public void setId(SecuritySettingKey id) {
        this.id = id;
    }

    public SecurityValue getValue() {
        return value;
    }

    public void setValue(SecurityValue value) {
        this.value = value;
    }
}