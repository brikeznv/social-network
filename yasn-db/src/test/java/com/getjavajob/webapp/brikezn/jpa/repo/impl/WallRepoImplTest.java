package com.getjavajob.webapp.brikezn.jpa.repo.impl;

import com.getjavajob.webapp.brikezn.jpa.bean.Profile;
import com.getjavajob.webapp.brikezn.jpa.bean.WallMessage;
import com.getjavajob.webapp.brikezn.jpa.repo.ProfileRepo;
import com.getjavajob.webapp.brikezn.jpa.repo.WallRepo;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

import java.math.BigInteger;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


@ContextConfiguration(locations = "classpath:repo-test-context.xml")
public class WallRepoImplTest extends AbstractTransactionalJUnit4SpringContextTests {

    @Autowired
    WallRepo wallRepo;

    @Autowired
    ProfileRepo profileRepo;

    private RowMapper<WallMessage> getWallMessageRowMapper() {
        return new RowMapper<WallMessage>() {
            @Override
            public WallMessage mapRow(ResultSet rs, int rowNum) throws SQLException {

                WallMessage wallMessage = new WallMessage();
                Profile sender = profileRepo.getProfile(rs.getInt("wall_sender"));
                wallMessage.setSender(sender);
                Profile recipient = profileRepo.getProfile(rs.getInt("wall_recipient"));
                wallMessage.setRecipient(recipient);
                wallMessage.setContent(rs.getString("wall_content"));

                return wallMessage;
            }
        };
    }

    @Test
    public void addWallMessage() {
        // create and save new wall message
        Profile sender = profileRepo.getProfile(3);
        Profile recipient = profileRepo.getProfile(4);
        String content = "Hi!";
        Date wallPostTime = new Date(System.currentTimeMillis());
        WallMessage wallMessage = new WallMessage(sender, recipient, content, wallPostTime);
        wallRepo.saveOrUpdate(wallMessage);

        //getting saved message
        List<WallMessage> wallMessageList = jdbcTemplate.query("SELECT * FROM wall WHERE wall_recipient = 4 " +
                "AND wall_date = " + wallPostTime.getTime(), getWallMessageRowMapper());
        WallMessage savedMessage = wallMessageList.get(0);

        // check that saving was well
        assertNotNull(savedMessage);
        assertEquals(3, (int) savedMessage.getSender().getId());
        assertEquals(4, (int) savedMessage.getRecipient().getId());
        assertEquals("Hi!", savedMessage.getContent());
    }

    @Test
    public void getWallMessageListByProfileId() {
        // fill wall message list with sender: profile(4) and recipient profile(5)

        for (int i = 0; i < 10; i++) {
            BigInteger date = new BigInteger(String.valueOf(System.currentTimeMillis() + i));
            jdbcTemplate.update("INSERT INTO wall (wall_sender, wall_recipient, wall_content, wall_date)" +
                    "VALUES (4, 5, \'msg\'," + date + ")");
        }

        // when invoked getWallMessageList(Integer profileId)
        List<WallMessage> wallMessageList = wallRepo.getWallMessageList(5);

        // then returning wall messages list of chosen recipient profile id
        assertEquals(10, wallMessageList.size());
    }
}