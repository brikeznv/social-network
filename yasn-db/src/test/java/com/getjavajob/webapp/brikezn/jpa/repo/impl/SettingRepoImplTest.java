package com.getjavajob.webapp.brikezn.jpa.repo.impl;

import com.getjavajob.webapp.brikezn.jpa.bean.SecuritySetting;
import com.getjavajob.webapp.brikezn.jpa.repo.SettingRepo;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;


@ContextConfiguration(locations = "classpath:repo-test-context.xml")
public class SettingRepoImplTest extends AbstractTransactionalJUnit4SpringContextTests {

    @Autowired
    SettingRepo settingRepo;

    @Test
    public void createSecuritySettings_returns_security_settings() {
        // creating new profile
        jdbcTemplate.update("INSERT INTO profile VALUES " +
                "(123, 'security@test.com', 'nick', 'Name', 'Surname', '', '2015/01/01', 1)");

        // when createSecuritySettings(Integer Profile id) called
        List<SecuritySetting> securitySettingList = settingRepo.createSecuritySettings(123);

        // then returning security Settings
        assertEquals(5, securitySettingList.size());
    }

    @Test
    public void getSecuritySettings_returns_security_settings() {
        // then getSecuritySettings(Integer profileId) called
        List<SecuritySetting> securitySettingList = settingRepo.getSecuritySettings(1);

        // then returning security Settings
        assertEquals(5, securitySettingList.size());
    }

    @Test
    public void updateSecuritySettings_returns_security_settings() {

        // creating new profile and security settings, default values for security is "all"
        jdbcTemplate.update("INSERT INTO profile VALUES " +
                "(2015, 'securityvalues@test.com', 'nick', 'Name', 'Surname', '', '2015/01/01', 1)");
        List<SecuritySetting> securitySettings = settingRepo.createSecuritySettings(999);

        // creating parameter map for update security settings
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("id", 2015);
        paramMap.put("profileView", 4);

        // get updated security settings and check that all values of security type is default "all",
        // besides profileView (should be "nobody")

        List<SecuritySetting> securitySettingList = settingRepo.updateSecuritySettings(paramMap);

        for (SecuritySetting securitySetting : securitySettingList) {
            final String typeName = securitySetting.getId().getSecurityType().getName();
            final String valueName = securitySetting.getValue().getName();

            if ("profileView".equals(typeName)) {
                assertEquals("nobody", valueName);
            } else {
                assertEquals("all", valueName);
            }
        }
    }
}