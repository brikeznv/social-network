package com.getjavajob.webapp.brikezn.jpa.repo.impl;

import com.getjavajob.webapp.brikezn.jpa.bean.Image;
import com.getjavajob.webapp.brikezn.jpa.repo.ImageRepo;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import static org.junit.Assert.assertEquals;


@ContextConfiguration(locations = "classpath:repo-test-context.xml")
public class ImageRepoImplTest extends AbstractTransactionalJUnit4SpringContextTests {

    @Autowired
    ImageRepo imageRepo;

    private RowMapper<Image> getImageRowMapper() {
        return new RowMapper<Image>() {
            @Override
            public Image mapRow(ResultSet rs, int rowNum) throws SQLException {
                int id = rs.getInt(1);
                byte[] bytes = rs.getBytes(2);
                return new Image(id, bytes);
            }
        };
    }

    @Test
    public void addImage_returning_image() {
        // creating byte array of string and it`s input stream
        byte[] expected = "image".getBytes();
        int id = imageRepo.saveOrUpdate(new Image(expected));

        // saving byte array and get get it from data base
        List<Image> images = jdbcTemplate.query("SELECT * FROM img WHERE img_id = " + id, getImageRowMapper());
        byte[] result = images.get(0).getBytes();

        // check equal of expected and saved bytes
        assertEquals(expected, result);
    }
}