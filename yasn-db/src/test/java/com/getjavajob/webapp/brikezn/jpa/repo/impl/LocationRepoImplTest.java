package com.getjavajob.webapp.brikezn.jpa.repo.impl;

import com.getjavajob.webapp.brikezn.jpa.bean.location.City;
import com.getjavajob.webapp.brikezn.jpa.bean.location.Country;
import com.getjavajob.webapp.brikezn.jpa.bean.location.Region;
import com.getjavajob.webapp.brikezn.jpa.repo.LocationRepo;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

import java.util.List;

import static org.junit.Assert.assertEquals;


@ContextConfiguration(locations = "classpath:repo-test-context.xml")
public class LocationRepoImplTest extends AbstractTransactionalJUnit4SpringContextTests {

    @Autowired
    LocationRepo locationRepo;

    @Test
    public void getCountryById_return_country() {
        // getting country "Russia" with id=1
        Country country = locationRepo.getCountry(1);

        // checking obtain the desired country
        assertEquals("Россия", country.getTitle());
    }

    @Test
    public void getAllCountries_return_list_of_countries() {
        //getting number of countries
        final int countriesCount = countRowsInTable("countries");

        // getting all countries
        List<Country> countries = locationRepo.getAllCountries();

        // checking wee got actual number of countries
        assertEquals(countriesCount, countries.size());
    }

    @Test
    public void getRegionById_return_region() {
        // getting region "Хефа" with id=1
        Region region = locationRepo.getRegion(1);

        // checking obtain the desired region
        assertEquals("Хефа", region.getTitle());
    }

    @Test
    public void getRegionsByCountry_return_list_of_regions() {
        //getting number of regions in country with id=1
        String query = "SELECT * FROM region WHERE country_id = 1";
        List<Region> list = jdbcTemplate.query(query,
                BeanPropertyRowMapper.newInstance(Region.class));
        int regionsCount = list.size();

        // getting all regions of country with id=1
        Country country = locationRepo.getCountry(1);
        List<Region> regions = locationRepo.getRegions(country);

        // checking wee got actual number of countries
        assertEquals(regionsCount, regions.size());
    }

    @Test
    public void getCityById_return_city() {
        // getting city "Зея" with id=1
        City city = locationRepo.getCity(1);

        // checking obtain the desired city
        assertEquals("Зея", city.getTitle());
    }

    @Test
    public void getCitiesByRegion_return_list_of_cities() {
        //getting number of cities in region with id=1
        String query = "SELECT * FROM  city WHERE region_id = 1";
        List<City> list = jdbcTemplate.query(query,
                BeanPropertyRowMapper.newInstance(City.class));
        int citiesCount = list.size();

        // getting all cities of region with id=1
        Region region = locationRepo.getRegion(1);
        List<City> cities = locationRepo.getCities(region);

        // checking wee got actual number of cities
        assertEquals(citiesCount, cities.size());
    }

    @Test
    public void getCitiesByCountry_return_list_of_cities() {
        //getting number of cities in country with id=1
        String query = "SELECT * FROM cities WHERE country_id = 1";
        List<City> list = jdbcTemplate.query(query,
                BeanPropertyRowMapper.newInstance(City.class));
        int citiesCount = list.size();

        // getting all cities of country with id=1
        Country country = locationRepo.getCountry(1);
        List<City> cities = locationRepo.getCities(country);

        // checking wee got actual number of cities
        assertEquals(citiesCount, cities.size());
    }
}