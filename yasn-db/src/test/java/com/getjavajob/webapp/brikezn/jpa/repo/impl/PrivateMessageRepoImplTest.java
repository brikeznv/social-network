package com.getjavajob.webapp.brikezn.jpa.repo.impl;

import com.getjavajob.webapp.brikezn.jpa.bean.PrivateMessage;
import com.getjavajob.webapp.brikezn.jpa.bean.Profile;
import com.getjavajob.webapp.brikezn.jpa.repo.PrivateMessageRepo;
import com.getjavajob.webapp.brikezn.jpa.repo.ProfileRepo;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

import java.math.BigInteger;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


@ContextConfiguration(locations = "classpath:repo-test-context.xml")
public class PrivateMessageRepoImplTest extends AbstractTransactionalJUnit4SpringContextTests {

    @Autowired
    PrivateMessageRepo privateMessageRepo;

    @Autowired
    ProfileRepo profileRepo;

    private RowMapper<PrivateMessage> getPrivateMessageRowMapper() {
        return new RowMapper<PrivateMessage>() {
            @Override
            public PrivateMessage mapRow(ResultSet rs, int rowNum) throws SQLException {

                PrivateMessage msg = new PrivateMessage();
                msg.setContent(rs.getString("msg_content"));

                return msg;
            }
        };
    }

    @Test
    public void savePrivateMessage() {

        //creating new private message
        Profile sender = profileRepo.getProfile(1);
        Profile recipient = profileRepo.getProfile(3);
        String expectedContent = "Test message!";
        PrivateMessage msg = new PrivateMessage(sender, recipient, expectedContent, new BigInteger("1"));

        //save private message into DB
        BigInteger id = privateMessageRepo.saveMessage(msg);

        //get private message with same id
        jdbcTemplate.query("SELECT * FROM private_msg WHERE msg_id = " + id, getPrivateMessageRowMapper());
        PrivateMessage result = privateMessageRepo.getMessage(id);

        //Check not null result
        assertNotNull(result);

        //Check expected and result content of messages are same
        String resultContent = result.getContent();
        assertEquals(expectedContent, resultContent);
    }
}
