package com.getjavajob.webapp.brikezn.jpa.repo.impl;

import com.getjavajob.webapp.brikezn.jpa.bean.Profile;
import com.getjavajob.webapp.brikezn.jpa.repo.ProfileRepo;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;


@ContextConfiguration(locations = "classpath:repo-test-context.xml")
public class ProfileRepoImplTest extends AbstractTransactionalJUnit4SpringContextTests {

    @Autowired
    ProfileRepo profileRepo;

    private RowMapper<Profile> getProfileRowMapper() {
        return new RowMapper<Profile>() {
            @Override
            public Profile mapRow(ResultSet rs, int rowNum) throws SQLException {

                Profile profile = new Profile();
                profile.setEmail(rs.getString("profile_email"));

                return profile;
            }
        };
    }

    @Test
    public void getProfiles_return_all_profiles() {
        // given count of Profiles
        final int numberOfProfiles = countRowsInTable("profile");

        // when getAllProfiles() called
        final List<Profile> profiles = profileRepo.getAllProfiles();

        // then returned all Profile beans
        assertEquals(numberOfProfiles, profiles.size());
    }

    @Test
    public void getProfileByEmail_return_profile() {
        // insert profile into table
        jdbcTemplate.update("INSERT INTO profile " +
                "(profile_id, profile_email, profile_password, profile_first_name, profile_last_name, profile_gender) " +
                "VALUES (100, 'testemail@test.com', 'pass', 'Name', 'Surname', 'm')");

        // when getProfile(String) is called
        final Profile profile = profileRepo.getProfile("testemail@test.com");

        // then profile returned, check profile id
        assertEquals(100, (int) profile.getId());
    }

    @Test
    public void getProfileById_return_profile() {
        // insert profile into table
        jdbcTemplate.update("INSERT INTO profile " +
                "(profile_id, profile_email, profile_password, profile_first_name, profile_last_name, profile_gender) " +
                "VALUES (100, 'testemail@test.com', 'pass', 'Name', 'Surname', 'm')");

        // when getProfile(Integer) is called
        final Profile profile = profileRepo.getProfile(100);

        // then profile returned, check email equal
        assertEquals("testemail@test.com", profile.getEmail());
    }


    @Test
    public void deleteProfile_return_null() {
        // insert profile into table
        int id = 112;
        jdbcTemplate.update("INSERT INTO profile " +
                "(profile_id, profile_email, profile_password, profile_first_name, profile_last_name, profile_gender) " +
                "VALUES (" + id + ", 'testdelete@test.com', 'pass', 'Name', 'Surname', 'm')");

        // check profile saved well and removing it
        assertNotNull(profileRepo.getProfile(id));
        profileRepo.deleteProfile(id);

        // check that profile does not exist
        assertNull(profileRepo.getProfile(id));
    }

    @Test
    public void saveProfileByProfileBean_return_profile() {
        // creating new Profile
        final Profile profile = new Profile("bah@test.com", "pass", "Iogann", "Bah", "m",
                new Date(System.currentTimeMillis()));

        // when saveOrUpdate(Profile)
        profileRepo.saveOrUpdate(profile);
        final Profile savedProfile = profileRepo.getProfile("bah@test.com");

        // then returns profile
        assertEquals("Iogann", savedProfile.getFirstName());

    }

    @Test
    public void updateProfileByProfileBean() {
        // creating and save new Profile, returning id
        final Profile profile = new Profile("vagner@test.com", "pass", "Richard", "Vagner", "m",
                new Date(System.currentTimeMillis()));
        int id = profileRepo.saveOrUpdate(profile);

        //creating new profile for update
        final Profile updateProfile = new Profile();
        updateProfile.setId(id);
        updateProfile.setFirstName("Rich");

        // when saveOrUpdate(Profile) with not null id is called
        profileRepo.saveOrUpdate(updateProfile);

        // then created profile updating
        final Profile resultProfile = jdbcTemplate.query("SELECT * FROM profile " +
                "WHERE profile_email = \'vagner@test.com\'", getProfileRowMapper()).get(0);
        System.out.println(profile.getId());
        System.out.println(profile.getLastName());
        assertEquals("Rich", profile.getFirstName());
    }

    @Test
    public void searchProfiles_returns_list_of_profiles() {
        // creating map of search parameters with search query "dude" and getting profiles with name,
        // surname or nick name "dude"
        Map<String, Object> searchParamMap = new HashMap();
        searchParamMap.put("searchQuery", "Dude");
        List<Profile> expected = jdbcTemplate.query("SELECT * FROM profile WHERE profile_first_name = \'Dude\'" +
                "OR profile_last_name = \'Dude\' OR profile_nickname = \'Dude\'", getProfileRowMapper());

        // when profileRepo.search(Map<String, Object> paramMap) is called
        List<Profile> result = profileRepo.search(searchParamMap);

        // then returning list of profiles
        assertEquals(expected.size(), result.size());
    }
}