CREATE TABLE country (
  country_id SERIAL PRIMARY KEY NOT NULL,
  title      CHARACTER VARYING  NOT NULL
);

CREATE TABLE region (
  region_id  SERIAL PRIMARY KEY NOT NULL,
  country_id INTEGER            NOT NULL,
  title      CHARACTER VARYING  NOT NULL
);

CREATE TABLE city (
  city_id    SERIAL PRIMARY KEY NOT NULL,
  region_id  INTEGER            NOT NULL,
  country_id INTEGER            NOT NULL,
  title      CHARACTER VARYING
);

CREATE TABLE hobby (
  hobby_name VARCHAR UNIQUE     NOT NULL
);

CREATE TABLE profile (
  profile_id         SERIAL PRIMARY KEY NOT NULL,
  profile_email      VARCHAR UNIQUE     NOT NULL,
  profile_password   VARCHAR            NOT NULL,
  profile_first_name VARCHAR            NOT NULL,
  profile_last_name  VARCHAR            NOT NULL,
  profile_nickname   VARCHAR,
  profile_gender     VARCHAR            NOT NULL,
  profile_birthday   DATE,
  profile_city       INTEGER REFERENCES city (city_id)
);

CREATE TABLE hobby_profile (
  hobby_profile_id    SERIAL  NOT NULL PRIMARY KEY,
  hobby_profile_owner INTEGER NOT NULL REFERENCES profile (profile_id),
  hobby_profile_hobby INTEGER NOT NULL REFERENCES hobby (hobby_id)
);

CREATE TABLE relationship (
  relationship_id    SERIAL PRIMARY KEY NOT NULL,
  relationship_value VARCHAR UNIQUE     NOT NULL
);

CREATE TABLE relationship_profile (
  relationship_profile_object  INTEGER REFERENCES profile (profile_id),
  relationship_profile_subject INTEGER REFERENCES profile (profile_id),
  relationship_profile_value   INTEGER NOT NULL REFERENCES relationship (relationship_id),
  PRIMARY KEY (relationship_profile_object, relationship_profile_subject)
);

CREATE TABLE message (
  message_id        SERIAL PRIMARY KEY NOT NULL,
  message_sender    INTEGER            NOT NULL REFERENCES profile (profile_id),
  message_recipient INTEGER            NOT NULL REFERENCES profile (profile_id),
  message_content   VARCHAR            NOT NULL,
  message_date      DATE               NOT NULL,
  message_time      TIME               NOT NULL
);

CREATE TABLE wall (
  wall_id        SERIAL  NOT NULL PRIMARY KEY,
  wall_sender    INTEGER NOT NULL REFERENCES profile (profile_id),
  wall_recipient INTEGER NOT NULL REFERENCES profile (profile_id),
  wall_content   VARCHAR NOT NULL,
  wall_date      BIGINT  NOT NULL,
  UNIQUE (wall_sender, wall_recipient, wall_date)
);

CREATE TABLE setting (
  setting_id    SERIAL  NOT NULL PRIMARY KEY,
  setting_value VARCHAR NOT NULL
);

CREATE TABLE setting_profile (
  setting_profile_owner   INTEGER NOT NULL REFERENCES profile (profile_id),
  setting_profile_setting INTEGER NOT NULL REFERENCES setting (setting_id),
  setting_profile_value   VARCHAR NOT NULL
);

CREATE TABLE img (
  img_id   SERIAL NOT NULL PRIMARY KEY,
  img_path BYTEA  NOT NULL
);
--
CREATE TABLE avatar (
  avatar_profile_id INTEGER NOT NULL PRIMARY KEY REFERENCES profile (profile_id),
  avatar_img_id     INTEGER NOT NULL REFERENCES img (img_id)
);

CREATE TABLE security_value (
  security_value_id   INTEGER NOT NULL PRIMARY KEY,
  security_value_name VARCHAR NOT NULL
);

CREATE TABLE security_type (
  security_type_id   INTEGER NOT NULL PRIMARY KEY,
  security_type_name VARCHAR NOT NULL
);

CREATE TABLE security_profile (
  security_profile_id    INTEGER NOT NULL REFERENCES profile (profile_id),
  security_profile_type  INTEGER NOT NULL REFERENCES security_type (security_type_id),
  security_profile_value INTEGER NOT NULL REFERENCES security_value (security_value_id),
  UNIQUE (security_profile_id, security_profile_type)
);

CREATE TABLE private_msg (
  msg_id           BIGSERIAL NOT NULL PRIMARY KEY,
  msg_recipient_id INTEGER   NOT NULL REFERENCES profile (profile_id),
  msg_sender_id    INTEGER   NOT NULL REFERENCES profile (profile_id),
  msg_content      VARCHAR   NOT NULL,
  msg_date         BIGINT    NOT NULL
)