package com.getjavajob.webapp.brikezn.jpa.repo;

import com.getjavajob.webapp.brikezn.jpa.bean.Profile;

import java.util.Set;

/**
 * Created by Sierra.
 */
public interface RelationshipRepo {

    Set<Profile> getFriends(Integer profileId);
}
