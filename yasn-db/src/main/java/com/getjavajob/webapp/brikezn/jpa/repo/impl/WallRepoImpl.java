package com.getjavajob.webapp.brikezn.jpa.repo.impl;

import com.getjavajob.webapp.brikezn.jpa.bean.Profile;
import com.getjavajob.webapp.brikezn.jpa.bean.WallMessage;
import com.getjavajob.webapp.brikezn.jpa.repo.WallRepo;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;


@Repository
public class WallRepoImpl implements WallRepo {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public int saveOrUpdate(WallMessage wallMessage) {

        Integer id = wallMessage.getId();

        if (id != null && id > 0) {
            entityManager.merge(wallMessage);
            return 0;
        } else {
            entityManager.persist(wallMessage);
            return wallMessage.getId();
        }
    }

    @Override
    public List getWallMessageList(Integer id) {

        final Query query = entityManager.createQuery("select w from WallMessage w WHERE w.id.recipient.id = :id");
        query.setParameter("id", id);

        return query.getResultList();
    }

    @Override
    public List getWallMessages(Profile profile) {
        Integer id = profile.getId();
        return getWallMessageList(id);
    }

    @Override
    public void deleteWallMessage(int wallMessageId) {
        WallMessage wallMessage = entityManager.find(WallMessage.class, wallMessageId);
        entityManager.remove(wallMessage);
    }
}