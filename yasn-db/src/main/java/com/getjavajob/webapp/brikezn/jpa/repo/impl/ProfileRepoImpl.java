package com.getjavajob.webapp.brikezn.jpa.repo.impl;


import com.getjavajob.webapp.brikezn.DtoMapper;
import com.getjavajob.webapp.brikezn.jpa.bean.Avatar;
import com.getjavajob.webapp.brikezn.jpa.bean.Image;
import com.getjavajob.webapp.brikezn.jpa.bean.Profile;
import com.getjavajob.webapp.brikezn.jpa.repo.ProfileRepo;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.*;


@Repository
public class ProfileRepoImpl implements ProfileRepo {

    private static final Logger LOGGER = Logger.getLogger(ProfileRepoImpl.class);

    @PersistenceContext
    private EntityManager entityManager;


    public List<Profile> getAllProfiles() {
        final Query query = entityManager.createQuery("select p from Profile p");

        return query.getResultList();
    }

    public Profile getProfile(Integer id) {
        try {
            return entityManager.find(Profile.class, id);
        } catch (Exception e) {
            LOGGER.info("Profile bean does not found!", e);
        }

        return null;
    }

    public Profile getProfile(String email) {
        final Query query = entityManager.createQuery("select p from Profile p WHERE p.email = :email");
        query.setParameter("email", email);

        try {
            return (Profile) query.getSingleResult();
        } catch (Exception e) {
            LOGGER.info("Profile bean does not found!", e);
        }

        return null;
    }

    public void deleteProfile(Integer id) {
        Profile profile = entityManager.find(Profile.class, id);
        entityManager.remove(profile);
    }

    @Override
    public Integer saveOrUpdate(Profile profile) {
        Integer returnValue = 0;
        Object id = profile.getId();

        if (id == null) {
            entityManager.persist(profile);
            returnValue = profile.getId();
        } else {
            Profile profileTarget = entityManager.find(Profile.class, id);
            DtoMapper.fill(profileTarget, profile);
            entityManager.merge(profile);
        }

        return returnValue;
    }

    public List<Profile> search(Map<String, Object> paramMap) {

        EntityManager em = entityManager.getEntityManagerFactory().createEntityManager();
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Profile> query = criteriaBuilder.createQuery(Profile.class);
        Root<Profile> root = query.from(Profile.class);
        query.select(root);
        Predicate finalPredicate = getFinalPredicate(paramMap, root);
        query.where(criteriaBuilder.and(finalPredicate));
        TypedQuery<Profile> typedQuery = em.createQuery(query);
        String searchQuery = (String) paramMap.get("searchQuery");
        List<String> queryKeyWords;

        if (searchQuery != null && searchQuery.length() > 1) {
            queryKeyWords = getWordsListFromString(searchQuery);

            for (String keyWord : queryKeyWords) {
                typedQuery.setParameter(keyWord, "%" + keyWord + "%");
            }
        }

        Date birthdayStart = (Date) paramMap.get("birthdayStart");
        Date birthdayEnd = (Date) paramMap.get("birthdayEnd");

        if (birthdayStart != null) {
            typedQuery.setParameter("birthdayStart", birthdayStart);
        }

        if (birthdayEnd != null) {
            typedQuery.setParameter("birthdayEnd", birthdayEnd);
        }

        String gender = (String) paramMap.get("gender");

        if (gender != null && gender.length() > 0) {
            typedQuery.setParameter("gender", "%" + gender + "%");
        }

        List<Profile> profileList = typedQuery.getResultList();
        em.close();

        return profileList;
    }

    private Predicate getFinalPredicate(Map<String, Object> paramMap, Root root) {

        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        String searchQuery = (String) paramMap.get("searchQuery");
        List<Predicate> predicateList = new ArrayList<>();


        if (searchQuery != null && searchQuery.length() > 1) {
            predicateList.add(getQueryPredicate(searchQuery, root));
        }

        if (paramMap.get("birthdayStart") != null || paramMap.get("birthdayEnd") != null) {
            predicateList.add(getAgePredicate(paramMap, root));
        }

        String gender = (String) paramMap.get("gender");

        if (gender != null && gender.length() > 0) {
            predicateList.add(getGenderPredicate(root));
        }

        int numOfPredicates = predicateList.size();
        Predicate[] predicateArray = new Predicate[numOfPredicates];

        for (int i = 0; i < numOfPredicates; i++) {
            predicateArray[i] = predicateList.get(i);
        }

        return criteriaBuilder.and(predicateArray);
    }

    private Predicate getGenderPredicate(Root root) {

        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        ParameterExpression<String> parameterGender = criteriaBuilder.parameter(String.class, "gender");
        return criteriaBuilder.like(root.<String>get("gender"), parameterGender);
    }

    private Predicate getQueryPredicate(String searchQuery, Root root) {

        List<String> keyWords = getWordsListFromString(searchQuery);
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        Predicate[] predicateList = new Predicate[keyWords.size()];
        Predicate finalPredicate;
        int i = 0;

        for (String keyWord : keyWords) {
            ParameterExpression<String> parameterName = criteriaBuilder.parameter(String.class, keyWord);
            Predicate predicateFirstName = criteriaBuilder.like(root.<String>get("firstName"), parameterName);
            Predicate predicateLastName = criteriaBuilder.like(root.<String>get("lastName"), parameterName);
            Predicate predicateNickname = criteriaBuilder.like(root.<String>get("nickname"), parameterName);
            Predicate predicateCity = criteriaBuilder.like(
                    root.<String>get("city").get("title"), parameterName);
            Predicate predicateCountry = criteriaBuilder.like(
                    root.<String>get("city").get("country").get("title"), parameterName);
            Predicate joinNamePredicate = criteriaBuilder.or(predicateFirstName, predicateLastName,
                    predicateNickname, predicateCity, predicateCountry);
            predicateList[i] = joinNamePredicate;
            i++;
        }

        finalPredicate = criteriaBuilder.and(predicateList);

        return finalPredicate;
    }

    private Predicate getAgePredicate(Map<String, Object> paramMap, Root root) {

        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        List<Predicate> predicateList = new ArrayList<>();
        Date birthdayStart = (Date) paramMap.get("birthdayStart");
        Date birthdayEnd = (Date) paramMap.get("birthdayEnd");

        if (birthdayStart != null) {
            ParameterExpression<Date> expression = criteriaBuilder.parameter(Date.class, "birthdayStart");
            predicateList.add(criteriaBuilder.lessThanOrEqualTo(root.<Date>get("birthday"), expression));
        }

        if (birthdayEnd != null) {
            ParameterExpression<Date> expression = criteriaBuilder.parameter(Date.class, "birthdayEnd");
            predicateList.add(criteriaBuilder.greaterThanOrEqualTo(root.<Date>get("birthday"), expression));
        }

        return criteriaBuilder.and((Predicate[]) predicateList.toArray());


    }


    private List<String> getWordsListFromString(String query) {
        ArrayList<String> keyWords = new ArrayList<>();
        Collections.addAll(keyWords, query.split(" "));

        return keyWords;
    }

    @Transactional
    public void updateProfileImage(Integer profileId, Image img) {
        Avatar avatar;

        try {
            Query query = entityManager.createQuery("select a from Avatar a where a.owner.id = :profileId")
                    .setParameter("profileId", profileId);
            avatar = (Avatar) query.getSingleResult();
            avatar.setImage(img);
            entityManager.merge(avatar);
        } catch (Exception e) {

            Profile profile = getProfile(profileId);
            avatar = new Avatar(profile, img);
            entityManager.persist(avatar);
        }
    }
}