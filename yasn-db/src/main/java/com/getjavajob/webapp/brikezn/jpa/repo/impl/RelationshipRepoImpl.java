package com.getjavajob.webapp.brikezn.jpa.repo.impl;

import com.getjavajob.webapp.brikezn.jpa.bean.Profile;
import com.getjavajob.webapp.brikezn.jpa.repo.RelationshipRepo;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Set;

/**
 * Created by Sierra.
 */
public class RelationshipRepoImpl implements RelationshipRepo {

    @PersistenceContext
    private EntityManager em;


    @Override
    public Set<Profile> getFriends(Integer profileId) {

        Profile profile = em.find(Profile.class, profileId);

        return profile.getFriends();
    }
}
