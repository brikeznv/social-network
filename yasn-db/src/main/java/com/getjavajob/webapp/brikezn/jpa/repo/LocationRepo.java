package com.getjavajob.webapp.brikezn.jpa.repo;

import com.getjavajob.webapp.brikezn.jpa.bean.location.City;
import com.getjavajob.webapp.brikezn.jpa.bean.location.Country;
import com.getjavajob.webapp.brikezn.jpa.bean.location.Region;

import java.util.List;


public interface LocationRepo {

    Country getCountry(Integer id);

    List<Country> getAllCountries();

    Region getRegion(Integer id);

    List<Region> getRegions(Country country);

    City getCity(Integer id);

    List<City> getCities(Region region);

    List<City> getCities(Country country);
}