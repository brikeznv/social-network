package com.getjavajob.webapp.brikezn.jpa.repo.impl;

import com.getjavajob.webapp.brikezn.jpa.bean.location.City;
import com.getjavajob.webapp.brikezn.jpa.bean.location.Country;
import com.getjavajob.webapp.brikezn.jpa.bean.location.Region;
import com.getjavajob.webapp.brikezn.jpa.repo.LocationRepo;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;


@Repository
public class LocationRepoImpl implements LocationRepo {

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public Country getCountry(Integer id) {
        return entityManager.find(Country.class, id);
    }

    @Transactional
    public List<Country> getAllCountries() {
        Query query = entityManager.createQuery("SELECT c FROM Country c");

        return query.getResultList();
    }

    @Transactional
    public Region getRegion(Integer id) {
        return entityManager.find(Region.class, id);
    }

    @Transactional
    public List<Region> getRegions(Country country) {
        Query query = entityManager.createQuery("SELECT r FROM Region r WHERE r.country = :targetCountry");
        query.setParameter("targetCountry", country);

        return query.getResultList();
    }

    @Transactional
    public City getCity(Integer id) {
        return entityManager.find(City.class, id);
    }

    @Transactional
    public List<City> getCities(Region region) {
        Query query = entityManager.createQuery("SELECT c FROM City c WHERE c.region = :targetRegion");
        query.setParameter("targetRegion", region);

        return query.getResultList();
    }

    @Transactional
    public List<City> getCities(Country country) {
        Query query = entityManager.createQuery("SELECT c FROM City c WHERE c.country = :targetCountry");
        query.setParameter("targetCountry", country);

        return query.getResultList();
    }
}