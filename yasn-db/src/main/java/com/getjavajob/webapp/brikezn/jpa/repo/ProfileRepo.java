package com.getjavajob.webapp.brikezn.jpa.repo;


import com.getjavajob.webapp.brikezn.jpa.bean.Image;
import com.getjavajob.webapp.brikezn.jpa.bean.Profile;

import java.util.List;
import java.util.Map;

public interface ProfileRepo {

    List<Profile> getAllProfiles();

    Profile getProfile(Integer id);

    Profile getProfile(String email);

    void deleteProfile(Integer id);

    Integer saveOrUpdate(Profile profile);

    List<Profile> search(Map<String, Object> paramMap);

    void updateProfileImage(Integer profileId, Image img);
}