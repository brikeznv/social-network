package com.getjavajob.webapp.brikezn.jpa.repo;

import com.getjavajob.webapp.brikezn.jpa.bean.Image;


public interface ImageRepo {

    int saveOrUpdate(Image img);
}