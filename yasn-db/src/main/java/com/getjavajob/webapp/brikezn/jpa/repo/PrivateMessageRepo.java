package com.getjavajob.webapp.brikezn.jpa.repo;

import com.getjavajob.webapp.brikezn.jpa.bean.PrivateMessage;

import java.math.BigInteger;
import java.util.List;


public interface PrivateMessageRepo {

    List<PrivateMessage> getMessages(Integer profileId, Integer memberId);

    BigInteger saveMessage(PrivateMessage privateMessage);

    PrivateMessage getMessage(BigInteger id);
}
