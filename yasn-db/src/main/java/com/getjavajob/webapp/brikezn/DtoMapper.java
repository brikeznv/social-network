package com.getjavajob.webapp.brikezn;

import org.apache.log4j.Logger;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.util.Map;


public class DtoMapper {

    private static final Logger LOGGER = Logger.getLogger(DtoMapper.class);

    public <T> T map(String tableName, ResultSet rs, Class<T> clazz) {
        T dto = null;
        Field[] declaredFields = clazz.getDeclaredFields();


        try {
            dto = clazz.getDeclaredConstructor().newInstance();
        } catch (ReflectiveOperationException e) {
            LOGGER.error("No actual declared constructor.", e);
        }

        for (Field declaredField : declaredFields) {

            try {
                String fieldName = declaredField.getName();
                String columnLabel = makeColumnName(fieldName);
                String columnName = tableName + "_" + columnLabel;
                String fieldSetter = makeFieldSetterName(fieldName);
                Object o = rs.getObject(columnName);

                if (o != null) {
                    Method set = clazz.getDeclaredMethod(fieldSetter, o.getClass());
                    set.invoke(dto, o);
                }

            } catch (Exception e) {
                LOGGER.error("Mapping exception!", e);
            }
        }

        return dto;
    }

    public static <T> T mapByAttributes(Map attributes, Class<T> clazz) {
        T dto = null;
        Field[] declaredFields = clazz.getDeclaredFields();

        try {
            dto = clazz.getDeclaredConstructor().newInstance();
        } catch (ReflectiveOperationException e) {
            LOGGER.error("Class does not have daclared constructor without parameters!", e);
        }

        for (Field declaredField : declaredFields) {
            try {
                String fieldName = declaredField.getName();
                String fieldSetter = makeFieldSetterName(fieldName);
                Object value = attributes.get(fieldName);

                if (value != null) {
                    Method set = clazz.getDeclaredMethod(fieldSetter, value.getClass());
                    set.invoke(dto, value);
                }

            } catch (ReflectiveOperationException e) {
                LOGGER.error("Cant map field!", e);
            }
        }

        return dto;
    }

    public static <T> T mapByAttributesPrimary(Map<String, Object> attributes, Class<T> clazz) {
        T dto = null;

        try {
            dto = clazz.getDeclaredConstructor().newInstance();
        } catch (ReflectiveOperationException e) {
            LOGGER.error("Class does not have daclared constructor without parameters!", e);
        }

        for (String fieldName : attributes.keySet()) {
            try {
                String fieldSetter = makeFieldSetterName(fieldName);
                Object value = attributes.get(fieldName);

                if (value != null) {
                    Method set = clazz.getDeclaredMethod(fieldSetter, value.getClass());
                    set.invoke(dto, value);
                }

            } catch (ReflectiveOperationException e) {
                LOGGER.error("Cant map field!", e);
            }
        }

        return dto;
    }

    public static String makeColumnName(String fieldName) {

        if (fieldName == null || fieldName.length() == 0) {
            return "";
        }

        StringBuilder builder = new StringBuilder();
        char[] chars = fieldName.toCharArray();

        for (char c : chars) {
            if (Character.isUpperCase(c)) {
                builder.append('_');
                builder.append(Character.toLowerCase(c));
            } else {
                builder.append(c);
            }
        }

        return builder.toString();
    }

    public static <T> T fill(T obj, Map<String, Object> attributes) {
        Class clazz = obj.getClass();

        for (String fieldName : attributes.keySet()) {
            try {
                String fieldSetter = makeFieldSetterName(fieldName);
                Object value = attributes.get(fieldName);

                if (value != null) {
                    Method set = clazz.getDeclaredMethod(fieldSetter, value.getClass());
                    set.invoke(obj, value);
                }
            } catch (ReflectiveOperationException e) {
                LOGGER.error("Cant map field!", e);
            }
        }

        return obj;
    }

    public static <T> void fill(T target, T subject) {
        Class clazz = target.getClass();
        Field[] declaredFields = clazz.getDeclaredFields();

        for (Field declaredField : declaredFields) {
            try {
                String fieldSetter = makeFieldSetterName(declaredField.getName());
                String fieldGetter = makeFieldGetter(declaredField.getName());
                Method get = clazz.getDeclaredMethod(fieldGetter, Object.class);
                Object value = get.invoke(subject);

                if (value != null) {
                    Method set = clazz.getDeclaredMethod(fieldSetter, value.getClass());
                    set.invoke(target, value);
                }
            } catch (ReflectiveOperationException e) {
                LOGGER.error("Cant map field!", e);
            }

        }
    }

    public static String makeFieldSetterName(String fieldName) {
        return "set" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
    }

    public static String makeFieldGetter(String fieldName) {
        return "get" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
    }
}