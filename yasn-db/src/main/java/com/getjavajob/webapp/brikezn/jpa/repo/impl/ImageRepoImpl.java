package com.getjavajob.webapp.brikezn.jpa.repo.impl;

import com.getjavajob.webapp.brikezn.jpa.bean.Image;
import com.getjavajob.webapp.brikezn.jpa.repo.ImageRepo;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


@Repository
public class ImageRepoImpl implements ImageRepo {

    private final static Logger LOGGER = Logger.getLogger(ImageRepoImpl.class);

    @PersistenceContext
    EntityManager entityManager;

    @Override
    public int saveOrUpdate(Image img) {

        Integer id = img.getId();

        if (id != null && id > 0) {
            entityManager.merge(img);
        } else {
            entityManager.persist(img);
            return img.getId();
        }

        return 0;
    }
}