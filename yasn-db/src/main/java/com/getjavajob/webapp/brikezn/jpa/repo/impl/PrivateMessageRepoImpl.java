package com.getjavajob.webapp.brikezn.jpa.repo.impl;

import com.getjavajob.webapp.brikezn.jpa.bean.PrivateMessage;
import com.getjavajob.webapp.brikezn.jpa.repo.PrivateMessageRepo;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.List;


@Repository
public class PrivateMessageRepoImpl implements PrivateMessageRepo {

    @PersistenceContext
    EntityManager entityManager;

    public List<PrivateMessage> getMessages(Integer profileId, Integer memberId) {

        Query query = entityManager.createQuery("select pm from PrivateMessage pm where pm.recipient.id = :profileId " +
                "and pm.sender.id = :memberId or pm.recipient.id = :memberId and pm.sender.id = :profileId");
        query.setParameter("profileId", profileId);
        query.setParameter("memberId", memberId);
        return query.getResultList();
    }

    @Override
    public BigInteger saveMessage(PrivateMessage privateMessage) {

        entityManager.persist(privateMessage);
        return privateMessage.getId();
    }

    @Override
    public PrivateMessage getMessage(BigInteger id) {
        return entityManager.find(PrivateMessage.class, id);
    }
}
