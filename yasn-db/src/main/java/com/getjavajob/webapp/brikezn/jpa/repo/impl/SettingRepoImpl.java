package com.getjavajob.webapp.brikezn.jpa.repo.impl;

import com.getjavajob.webapp.brikezn.jpa.bean.Profile;
import com.getjavajob.webapp.brikezn.jpa.bean.SecuritySetting;
import com.getjavajob.webapp.brikezn.jpa.bean.SecurityType;
import com.getjavajob.webapp.brikezn.jpa.bean.SecurityValue;
import com.getjavajob.webapp.brikezn.jpa.bean.SecuritySettingKey;
import com.getjavajob.webapp.brikezn.jpa.repo.SettingRepo;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Repository
public class SettingRepoImpl implements SettingRepo {

    private final static Logger LOGGER = Logger.getLogger(SettingRepoImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public List<SecuritySetting> createSecuritySettings(Integer profileId) {
        Profile profile = entityManager.find(Profile.class, profileId);
        final Query query = entityManager.createQuery("select s from SecurityType s");
        final List<SecurityType> securityTypeList = query.getResultList();
        final SecurityValue defaultSecurityValue = entityManager.find(SecurityValue.class, 1);
        List<SecuritySetting> securitySettingList = new ArrayList<>();

        for (SecurityType securityType : securityTypeList) {
            SecuritySettingKey ssk = new SecuritySettingKey(profile, securityType);
            SecuritySetting setting = new SecuritySetting(ssk, defaultSecurityValue);
            entityManager.persist(setting);
            securitySettingList.add(setting);
        }

        return securitySettingList;
    }

    public List<SecuritySetting> getSecuritySettings(Integer profileId) {
        final Query query = entityManager.createQuery("select s from SecuritySetting s WHERE s.id.owner.id = :profileId");
        query.setParameter("profileId", profileId);

        return query.getResultList();
    }

    @Transactional
    public List<SecuritySetting> updateSecuritySettings(Map<String, Object> paramMap) {
        final Query query = entityManager.createQuery("select s from SecuritySetting s WHERE s.id.owner.id = :profileId");
        Integer profileId = (Integer) paramMap.get("id");
        query.setParameter("profileId", profileId);
        List<SecuritySetting> securitySettingList = query.getResultList();
        List<SecuritySetting> updatedSecuritySettingList = new ArrayList<>();

        for (SecuritySetting securitySetting : securitySettingList) {

            try {
                String settingName = securitySetting.getId().getSecurityType().getName();
                String settingValueName = (String) paramMap.get(settingName);
                final Query q = entityManager.createQuery("select s from SecurityValue s where s.name = :valueName");
                q.setParameter("valueName", settingValueName);
                SecurityValue securityValue = (SecurityValue) q.getResultList().get(0);
                securitySetting.setValue(securityValue);
                entityManager.merge(securitySetting);
                updatedSecuritySettingList.add(securitySetting);
            } catch (ClassCastException e) {
                LOGGER.error(e);
            }
        }

        return updatedSecuritySettingList;
    }
}