package com.getjavajob.webapp.brikezn.jpa.repo;

import com.getjavajob.webapp.brikezn.jpa.bean.SecuritySetting;

import java.util.List;
import java.util.Map;


public interface SettingRepo {

    List<SecuritySetting> createSecuritySettings(Integer profileId);

    List<SecuritySetting> getSecuritySettings(Integer profileId);

    List<SecuritySetting> updateSecuritySettings(Map<String, Object> paramMap);
}