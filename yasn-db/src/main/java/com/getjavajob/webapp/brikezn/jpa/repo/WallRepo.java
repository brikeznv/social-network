package com.getjavajob.webapp.brikezn.jpa.repo;

import com.getjavajob.webapp.brikezn.jpa.bean.Profile;
import com.getjavajob.webapp.brikezn.jpa.bean.WallMessage;

import java.util.List;


public interface WallRepo {

    int saveOrUpdate(WallMessage wallMessage);

    List getWallMessageList(Integer id);

    List getWallMessages(Profile profile);

    void deleteWallMessage(int wallMessageId);
}