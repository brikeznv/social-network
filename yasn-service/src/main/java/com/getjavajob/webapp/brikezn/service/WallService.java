package com.getjavajob.webapp.brikezn.service;

import java.util.List;
import java.util.Map;

public interface WallService {

    List getWallMessageList(Integer profileId);

    void addWallMessage(Map<String, Object> wallMessageParam);

    void deleteWallMessage(int wallMessageId);
}
