package com.getjavajob.webapp.brikezn.service.impl;

import com.getjavajob.webapp.brikezn.jpa.bean.Profile;
import com.getjavajob.webapp.brikezn.jpa.bean.SecuritySetting;
import com.getjavajob.webapp.brikezn.jpa.repo.ProfileRepo;
import com.getjavajob.webapp.brikezn.jpa.repo.SettingRepo;
import com.getjavajob.webapp.brikezn.service.SettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
public class SettingServiceImpl implements SettingService {

    @Autowired
    SettingRepo settingRepo;

    @Autowired
    ProfileRepo profileRepo;

    @Override
    @Transactional
    public void createSecuritySettings(int id) {
        settingRepo.createSecuritySettings(id);
    }

    @Override
    @Transactional
    public void updateProfileSecurity(Map<String, Object> paramMap) {
        settingRepo.updateSecuritySettings(paramMap);
    }

    @Override
    public List<SecuritySetting> getProfileSecurity(int id) {
        return settingRepo.getSecuritySettings(id);
    }

    @Override
    public Map<String, Boolean> getAccessParameters(Integer loggedUserId, Integer profileId) {

        if (loggedUserId.equals(profileId)) {
            Map<String, Boolean> accessParamMap = new HashMap<>();
            accessParamMap.put("pageView", true);
            accessParamMap.put("wallView", true);
            accessParamMap.put("friendListView", true);
            accessParamMap.put("messagePost", true);
            accessParamMap.put("wallPost", true);
            return accessParamMap;
        }

        Map<String, Boolean> accessParamMap = new HashMap<>();
        String expectedViewParam = "all";

        if (loggedUserId != null) {

            Profile profile = null;
            try {
                profile = profileRepo.getProfile(loggedUserId);

                if (profile != null) {
                    expectedViewParam = "registred";
                }
            } catch (Exception e) {
                expectedViewParam = "all";
            }

            if (profile != null) {
                Set<Profile> friends = profile.getFriends();

                for (Profile friend : friends) {
                    if (friend.getId().equals(profileId)) {
                        expectedViewParam = "friends";
                        break;
                    }
                }
            }

        } else {
            expectedViewParam = "all";
        }

        List<SecuritySetting> profileSecurityList = getProfileSecurity(profileId);

        for (SecuritySetting setting : profileSecurityList) {
            String name = setting.getId().getSecurityType().getName();
            String value = setting.getValue().getName();

            switch (value) {
                case "none":
                    accessParamMap.put(name, false);
                    break;
                case "friends":
                    boolean isFriend = expectedViewParam.equals(value);
                    accessParamMap.put(name, isFriend);
                    break;
                case "registred":
                    boolean isRegistred = expectedViewParam.equals(value);
                    accessParamMap.put(name, isRegistred);
                    break;
                case "all":
                    accessParamMap.put(name, true);
            }
        }

        return accessParamMap;
    }
}