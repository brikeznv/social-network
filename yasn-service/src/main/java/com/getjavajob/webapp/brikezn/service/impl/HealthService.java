package com.getjavajob.webapp.brikezn.service.impl;

import com.getjavajob.webapp.brikezn.DtoMapper;
import com.getjavajob.webapp.brikezn.jpa.bean.Profile;
import com.getjavajob.webapp.brikezn.jpa.repo.impl.ProfileRepoImpl;
import com.getjavajob.webapp.brikezn.jpa.repo.impl.SettingRepoImpl;
import com.getjavajob.webapp.brikezn.jpa.repo.impl.WallRepoImpl;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class HealthService {

    public HealthService() {
        super();
    }


    public void savePrivateMessage() {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("senderId", 1);
        paramMap.put("recipientId", 2);
        paramMap.put("content", "testing message");
        paramMap.put("date", BigInteger.valueOf(System.currentTimeMillis()));
    }

    public List<Profile> getProfiles() {
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("classpath:repo-context.xml");

        final ProfileRepoImpl profileRepo = context.getBean(ProfileRepoImpl.class);
        final List<Profile> allProfiles = profileRepo.getAllProfiles();
        for (Profile profile : allProfiles) {
            System.out.println(profile);
        }

        return allProfiles;
    }

    public Profile getProfile(Integer id) {

        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("classpath:repo-context.xml");
        final ProfileRepoImpl profileRepo = context.getBean(ProfileRepoImpl.class);

        return profileRepo.getProfile(id);
    }

    public List getAllWallMessages() {

        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("classpath:repo-context.xml");
        final WallRepoImpl wallRepo = context.getBean(WallRepoImpl.class);

        return wallRepo.getWallMessageList(1);
    }

    public void createSecuritySettings(Integer id) {

        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("classpath:repo-context.xml");
        final SettingRepoImpl settingRepo = context.getBean(SettingRepoImpl.class);
        settingRepo.createSecuritySettings(id);
    }

    public void testUpdateProfile() {


        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("classpath:repo-context.xml");
        final ProfileRepoImpl profileRepo = context.getBean(ProfileRepoImpl.class);
        Profile profile = profileRepo.getProfile(1);
        Map<String, Object> attributes = new HashMap<>();
        attributes.put("firstName", "Nickolyaa!!");
        attributes.put("lastName", "Brikesual");
        DtoMapper.fill(profile, attributes);
    }
}