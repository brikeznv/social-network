package com.getjavajob.webapp.brikezn.service;

import com.getjavajob.webapp.brikezn.jpa.bean.Profile;

import java.util.Set;

/**
 * Created by Sierra.
 */
public interface RelationshipService {

    public Set<Profile> getFriends(Integer profileId);
}
