package com.getjavajob.webapp.brikezn.service.impl;

import com.getjavajob.webapp.brikezn.DtoMapper;
import com.getjavajob.webapp.brikezn.jpa.bean.Image;
import com.getjavajob.webapp.brikezn.jpa.bean.Profile;
import com.getjavajob.webapp.brikezn.jpa.repo.ProfileRepo;
import com.getjavajob.webapp.brikezn.service.FileService;
import com.getjavajob.webapp.brikezn.service.ProfileService;
import com.getjavajob.webapp.brikezn.service.XMLConverter;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

@Service
public class ProfileServiceImpl implements ProfileService {

    private final static Logger LOGGER = Logger.getLogger(ProfileServiceImpl.class);

    @Autowired
    ProfileRepo profileRepo;

    @Autowired
    FileService fileService;

    @Override
    @Transactional
    public Integer saveOrUpdate(Profile profile) {
        return profileRepo.saveOrUpdate(profile);
    }

    @Override
    @Transactional
    public Integer saveOrUpdate(Map<String, Object> profileParam) {
        try {
            Profile profile = DtoMapper.mapByAttributes(profileParam, Profile.class);
            return profileRepo.saveOrUpdate(profile);
        } catch (ClassCastException e) {
            return -1;
        }
    }

    @Override
    public Profile get(String email) {
        return profileRepo.getProfile(email);
    }

    @Override
    public Profile get(Integer id) {
        return profileRepo.getProfile(id);
    }

    @Override
    public List<Profile> getAll() {
        return profileRepo.getAllProfiles();
    }

    @Override
    @Transactional
    public void delete(Integer id) {
        profileRepo.deleteProfile(id);
    }

    @Override
    @Transactional
    public void updateProfileImage(Integer profileId, InputStream in) {
        Image img = fileService.saveImage(in);

        if (img != null) {
            profileRepo.updateProfileImage(profileId, img);
        }
    }

    public <T> String getXMLFromBean(T bean) {
        return XMLConverter.getBeanXML(bean);
    }
}