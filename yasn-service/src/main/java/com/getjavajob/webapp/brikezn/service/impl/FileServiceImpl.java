package com.getjavajob.webapp.brikezn.service.impl;

import com.getjavajob.webapp.brikezn.jpa.bean.Image;
import com.getjavajob.webapp.brikezn.jpa.repo.ImageRepo;
import com.getjavajob.webapp.brikezn.service.FileService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

@Service
public class FileServiceImpl implements FileService {

    protected final static Logger LOGGER = Logger.getLogger(FileServiceImpl.class);

    @Autowired
    ImageRepo imageRepo;

    @Transactional
    public Image saveImage(InputStream in) {

        try (BufferedInputStream bufferedIn = new BufferedInputStream(in, 1024 * 10);
             ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            int val;

            while ((val = bufferedIn.read()) != -1) {
                out.write(val);
            }

            Image img = new Image(out.toByteArray());
            int saveResult = imageRepo.saveOrUpdate(img);

            if (saveResult > 0) {
                return img;
            }
        } catch (IOException e) {
            LOGGER.error(e);
        }

        return null;
    }
}