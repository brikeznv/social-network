package com.getjavajob.webapp.brikezn.service;

import com.getjavajob.webapp.brikezn.DtoMapper;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;


public class XMLConverter {

    private static final Logger LOGGER = Logger.getLogger(XMLConverter.class);

    public static <T> String getBeanXML(T bean) {

        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            Document doc = factory.newDocumentBuilder().newDocument();

            Class clazz = bean.getClass();
            String className = clazz.getName();
            Element root = doc.createElement(className);

            Field[] fields = clazz.getDeclaredFields();

            for (Field field : fields) {
                String fieldName = field.getName();
                Class<?> fieldType = field.getType();
                String fieldGetter = DtoMapper.makeFieldGetter(fieldName);

                try {
                    Method method = clazz.getDeclaredMethod(fieldGetter);
                    Object value = method.invoke(bean);

                    Element item = doc.createElement(fieldName);
                    item.setAttribute("type", fieldType.getName());
                    item.setAttribute("val", value.toString());
                    root.appendChild(item);
                } catch (ReflectiveOperationException e) {
                    LOGGER.error("No declared getter!", e);
                }
            }

            return doc.toString();
        } catch (ParserConfigurationException e) {
            LOGGER.error(e);
            return null;
        }
    }
}
