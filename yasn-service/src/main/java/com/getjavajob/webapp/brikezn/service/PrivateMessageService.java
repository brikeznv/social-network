package com.getjavajob.webapp.brikezn.service;

import com.getjavajob.webapp.brikezn.jpa.bean.PrivateMessage;

import java.util.List;
import java.util.Map;

public interface PrivateMessageService {

    public static final int COUNT_OF_MESSAGES_ON_PAGE = 10;

    List<PrivateMessage> get(Integer profileId, Integer memberId);

    List<PrivateMessage> get(Integer profileId, Integer memberId, Integer pageNumber);

    void save(Map<String, Object> paramMap);
}
