package com.getjavajob.webapp.brikezn.service;

import com.getjavajob.webapp.brikezn.jpa.bean.Profile;

import java.util.List;
import java.util.Map;

public interface SearchService {
    List<Profile> search(Map<String, String> paramMap);
}
