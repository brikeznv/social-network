package com.getjavajob.webapp.brikezn.service;

import com.getjavajob.webapp.brikezn.jpa.bean.SecuritySetting;

import java.util.List;
import java.util.Map;

public interface SettingService {

    void createSecuritySettings(int id);

    void updateProfileSecurity(Map<String, Object> paramMap);

    List<SecuritySetting> getProfileSecurity(int id);

    Map<String, Boolean> getAccessParameters(Integer loggedUserId, Integer profileId);
}
