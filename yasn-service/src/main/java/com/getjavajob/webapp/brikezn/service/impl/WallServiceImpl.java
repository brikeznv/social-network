package com.getjavajob.webapp.brikezn.service.impl;

import com.getjavajob.webapp.brikezn.DtoMapper;
import com.getjavajob.webapp.brikezn.jpa.bean.WallMessage;
import com.getjavajob.webapp.brikezn.jpa.repo.WallRepo;
import com.getjavajob.webapp.brikezn.service.WallService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;

@Service
public class WallServiceImpl implements WallService {

    @Autowired
    WallRepo wallRepo;

    @Override
    public List getWallMessageList(Integer profileId) {
        return wallRepo.getWallMessageList(profileId);
    }

    @Override
    @Transactional
    public void addWallMessage(Map<String, Object> wallMessageParam) {
        WallMessage wallMessage = DtoMapper.mapByAttributesPrimary(wallMessageParam, WallMessage.class);
        wallRepo.saveOrUpdate(wallMessage);
    }

    @Override
    @Transactional
    public void deleteWallMessage(int wallMessageId) {
        wallRepo.deleteWallMessage(wallMessageId);
    }
}