package com.getjavajob.webapp.brikezn.service;

import com.getjavajob.webapp.brikezn.jpa.bean.Image;

import java.io.InputStream;

public interface FileService {

    Image saveImage(InputStream in);
}
