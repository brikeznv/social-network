package com.getjavajob.webapp.brikezn.service.impl;

import com.getjavajob.webapp.brikezn.jpa.bean.location.City;
import com.getjavajob.webapp.brikezn.jpa.bean.location.Country;
import com.getjavajob.webapp.brikezn.jpa.bean.location.Region;
import com.getjavajob.webapp.brikezn.jpa.repo.LocationRepo;
import com.getjavajob.webapp.brikezn.service.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LocationServiceImpl implements LocationService {

    @Autowired
    LocationRepo locationRepo;

    @Override
    public City getCity(Integer id) {
        return locationRepo.getCity(id);
    }

    @Override
    public List<City> getCities(Region region) {
        return locationRepo.getCities(region);
    }

    @Override
    public List<City> getCities(Country country) {
        return locationRepo.getCities(country);
    }

    @Override
    public Region getRegion(Integer id) {
        return locationRepo.getRegion(id);
    }

    @Override
    public List<Region> getRegions(Country country) {
        return locationRepo.getRegions(country);
    }

    @Override
    public Country getCountry(Integer id) {
        return locationRepo.getCountry(id);
    }

    @Override
    public List<Country> getAllCountries() {
        return locationRepo.getAllCountries();
    }
}