package com.getjavajob.webapp.brikezn.service;

import com.getjavajob.webapp.brikezn.jpa.bean.Profile;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

public interface ProfileService {

    Integer saveOrUpdate(Profile profile);

    Integer saveOrUpdate(Map<String, Object> profileParam);

    Profile get(String email);

    Profile get(Integer id);

    List<Profile> getAll();

    void delete(Integer id);

    void updateProfileImage(Integer profileId, InputStream in);

    public <T> String getXMLFromBean(T bean);
}
