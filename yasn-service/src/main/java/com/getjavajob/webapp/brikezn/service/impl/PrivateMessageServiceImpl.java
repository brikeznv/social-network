package com.getjavajob.webapp.brikezn.service.impl;

import com.getjavajob.webapp.brikezn.jpa.bean.PrivateMessage;
import com.getjavajob.webapp.brikezn.jpa.bean.Profile;
import com.getjavajob.webapp.brikezn.jpa.repo.PrivateMessageRepo;
import com.getjavajob.webapp.brikezn.jpa.repo.ProfileRepo;
import com.getjavajob.webapp.brikezn.service.PrivateMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;

@Service
public class PrivateMessageServiceImpl implements PrivateMessageService {

    @Autowired
    PrivateMessageRepo privateMessageRepo;

    @Autowired
    ProfileRepo profileRepo;

    @Override
    public List<PrivateMessage> get(Integer profileId, Integer memberId) {
        return privateMessageRepo.getMessages(profileId, memberId);
    }

    @Override
    public List<PrivateMessage> get(Integer profileId, Integer memberId, Integer pageNumber) {

        List<PrivateMessage> privateMessageList = privateMessageRepo.getMessages(profileId, memberId);

        int msgSize = privateMessageList.size();
        int lastMessageIndex = pageNumber == 1 ? msgSize : msgSize - 10 * (pageNumber - 1);
        int firstMessageIndex = lastMessageIndex < 10 ? 0 : lastMessageIndex - 10;
        return privateMessageList.subList(firstMessageIndex, lastMessageIndex);
    }

    @Override
    @Transactional
    public void save(Map<String, Object> paramMap) {

        Integer senderId = (Integer) paramMap.get("senderId");
        Integer recipientId = Integer.valueOf((String) paramMap.get("recipientId"));
        Profile sender = profileRepo.getProfile(senderId);
        Profile recipient = profileRepo.getProfile(recipientId);
        String content = (String) paramMap.get("content");
        BigInteger date = BigInteger.valueOf((Long) paramMap.get("date"));
        PrivateMessage privateMessage = new PrivateMessage(sender, recipient, content, date);

        privateMessageRepo.saveMessage(privateMessage);
    }
}
