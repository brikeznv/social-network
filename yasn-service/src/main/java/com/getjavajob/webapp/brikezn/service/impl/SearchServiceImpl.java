package com.getjavajob.webapp.brikezn.service.impl;

import com.getjavajob.webapp.brikezn.jpa.bean.Profile;
import com.getjavajob.webapp.brikezn.jpa.repo.ProfileRepo;
import com.getjavajob.webapp.brikezn.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Service
public class SearchServiceImpl implements SearchService {

    @Autowired
    ProfileRepo profileRepo;

    @Override
    public List<Profile> search(Map<String, String> paramMap) {

        Map<String, Object> refactoredParamMap = new HashMap<>();
        refactoredParamMap.putAll(paramMap);
        String ageFrom = paramMap.get("ageFrom");
        String ageTo = paramMap.get("ageTo");

        if (ageFrom != null && ageFrom.length() > 0) {
            TimeUnit.DAYS.toMillis(365 * Long.parseLong(ageFrom));
            long ageFromMillis = TimeUnit.DAYS.toMillis(365 * Long.parseLong(ageFrom));
            refactoredParamMap.put("birthdayStart", new Date(System.currentTimeMillis() - ageFromMillis));
        }

        if (ageTo != null && ageTo.length() > 0) {
            long ageToMillis = TimeUnit.DAYS.toMillis(365 * Long.parseLong(ageTo));
            refactoredParamMap.put("birthdayEnd", new Date(System.currentTimeMillis() - ageToMillis));
        }

        return profileRepo.search(refactoredParamMap);
    }
}