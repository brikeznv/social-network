import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class TestService {

    @Test
    public void MapOutput() {
        Map<Integer, Object> test = new HashMap<>();
        Random rnd = new Random();

        for (int i = 0; i < 100; i++) {
            int value = rnd.nextInt(100);
            test.put(value, null);
        }

        for (Integer key : test.keySet()) {
            System.out.println(key);
        }
    }

    @Test
    public void test() {
        long i = 20;
        System.out.println(i + " age");
        i *= 365;
        System.out.println(i + " days");
        i = i * 24 * 60 * 60 * 100;
        System.out.println(i + " millis");
        System.out.println("current millis: " + System.currentTimeMillis());
        System.out.println();
        System.out.println(System.currentTimeMillis() / 100 / 60 / 60 / 24 / 365 - (System.currentTimeMillis() - i) / 100 / 60 / 60 / 24 / 365);
    }
}
